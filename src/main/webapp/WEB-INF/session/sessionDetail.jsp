<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageDetail.css">
<meta charset="UTF-8">
<title>Détails session</title>
</head>
<body>
<%@ include file="../header.jspf" %>
<div id="container">
<main>
	<div class="body" id="firstBody">
		<h1>Session # ${session.id}</h1>
		<h2>Intitulé : ${session.catalogue.titre}</h2>
		<div class="autreBouton">
			<form class="boutonDetail" action="catalogueDetail" method="post">
				<input type="hidden" value="${session.catalogue.id}" name="catalogueId"/>
				<input class="submit submitVoir" type="submit" value="Voir formation du catalogue"/>
			</form>
		</div>
		<table>
			<tr>
				<th>Nombre minimum de stagiaire :</th>
				<td>${session.catalogue.nbStagiaireMin}</td>
			</tr>
			<tr>
				<th>Nombre de stagiaire inscrits :</th>
				<td>${session.nbStagiaire}</td>
			</tr>
			<tr>
				<th>Nombre maximum de stagiaire :</th>
				<td>${session.catalogue.nbStagiaireMax}</td>
			</tr>
			<tr>
				<th>Date de début :</th>
				<td>${session.dateDebutFR}</td>
			</tr>
			<tr>
				<th>Date de fin :</th>
				<td>${session.dateFinFR}</td>
			</tr>
			<tr>
				<th>Formateur :</th>
				<td>${session.formateur.prenom} ${session.formateur.nom}</td>
			</tr>
			<tr>
				<td>
					<form class="boutonDetail" action="formateurDetail" method="post">
						<input type="hidden" value="${session.formateur.id}" name="formateurId"/>
						<input class="submit submitVoir" type="submit" value="Voir formateur"/>
					</form>
				</td>
			</tr>
			<tr>
				<th>Type de formation :</th>
				<td>${session.typeFormation}</td>
			</tr>
			<tr>
				<th>Salle :</th>
				<td>${session.salle.nom}</td>
			</tr>
			<tr>
				<td>
					<form class="boutonDetail" action="salleDetail" method="post">
						<input type="hidden" value="${session.salle.id}" name="salleId"/>
						<input class="submit submitVoir" type="submit" value="Voir salle"/>
					</form>
				</td>
			</tr>
			<tr>
				<th>Responsable de la formation :</th>
				<td>${session.responsable.email}</td>
			</tr>
		</table>
		<div class="actions">
			<form action="sessionModifier" method="get">
				<input type="hidden" name="id" value="${session.id}"/>
				<%-- <input type="hidden" name="catalogueId" value="${session.catalogue.id}"/>
				<input type="hidden" name="formateurId" value="${session.formateur.id}"/>
				<input type="hidden" name="salleId" value="${session.salle.id}"/>
				<input type="hidden" name="responsableId" value="${session.responsable.id}"/>
				<input type="hidden" name="dateDebut" value="${session.dateDebut}"/>
				<input type="hidden" name="dateFin" value="${session.dateFin}"/>
				<input type="hidden" name="typeFormation" value="${session.typeFormation}"/>
				<input type="hidden" name="nbStagiaire" value="${session.nbStagiaire}"/>--%>
				<input class="submitModifier" type="submit" value="Modifier la session"/>
			</form>
			<form action="sessionSupprimer" method="post">
				<input type="hidden" name="id" value="${session.id}"/>
				<input class="submitSupprimer" type="submit" value="Supprimer la session"/>
			</form>
		</div>
		<a id="retour" href="sessionListe">Retour</a>
	</div>
</main>
</div>

</body>
<%@ include file="../footer.jspf" %> 
</html>
