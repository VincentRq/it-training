<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste des Sessions de formation</title>
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageListe.css">
</head>
<%@ include file="../header.jspf" %>
<body>
<div id="container">
<main>
	<div class="body" id="firstBody">
		<a id="addNew" href="sessionAjouter"><span id="link">Ajouter une nouvelle session<span id="plus">+</span></span></a>
		<section id="sessions">
			<h1>Liste des Sessions de formation</h1>
			<table border="1">
				<thead>
					<tr>
						<th>Intitulé</th>
						<th>Date de début</th>
						<th>Date de fin</th>
						<th>Formateur</th>
						<th>Nombre de stagaire<br/>Min/inscrit/Max</th>
						<th>Type de formation</th>
						<th>Salle</th>
						<th>Détails</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${sessions}" var="session">
						<tr>
							<td>${session.catalogue.titre}</td>
							<td>${session.dateDebutFR}</td>
							<td>${session.dateFinFR}</td>
							<td>${session.formateur.nom} ${session.formateur.prenom}</td>
							<td>${session.catalogue.nbStagiaireMin} / ${session.nbStagiaire} / ${session.catalogue.nbStagiaireMax}</td>
							<td>${session.typeFormation}</td>
							<td>Salle ${session.salle.nom} à ${session.salle.ville}</td>
							<td>
								<form class="boutonDetail" action="sessionDetail" method="post">
									<input type="hidden" value="${session.id}" name="sessionId"/>
									<input class="submit" type="submit" value="Voir la session"/>
								</form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</section>
		<br/>
		<a class="bouton"id="retour" href="tableauDeBordAdmin">Retour au tableau de bord</a>
	</div>
</main>
</div>
<%@ include file="../footer.jspf" %> 
</body>
</html>