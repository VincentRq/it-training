<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>IT-Training - Créer une session</title>
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageAjouterModifier.css">       
</head>
<body>
<%@ include file="../header.jspf" %>

<div id="container">
<main>
	<div class="body" id="firstBody">
		<h1>Modifier la session</h1>
		<div id="form">
			<form action="sessionModifier" method="post">
				<label for="nom">Intitulé de la session :</label>
				<select id="catalogue" name="catalogueId">
				    <c:forEach items="${catalogues}" var="catalogue">
				    	<option value="${catalogue.id}" ${catalogue.id == session.catalogue.id ? 'selected = "selected"' : ''}>${catalogue.domaine} - ${catalogue.titre}</option>
				    </c:forEach>
			  	</select><br/>
				<label for="dateDebut">Date de début :</label>
				<input type="date" value="${session.dateDebut}" id="dateDebut" name="dateDebut"/><br/>
				<label for="dateFin">Date de fin :</label>
				<input type="date" value="${session.dateFin}" id="dateFin" name="dateFin"/><br/>
				<label for="formateur">Formateur proposé :</label>
			  	<select id="formateur" name="formateurId">
				    <c:forEach items="${formateurs}" var="formateur">
				    	<option value="${formateur.id}" ${formateur.id == session.formateur.id ? 'selected = "selected"' : ''}>${formateur.nom} ${formateur.prenom}</option>
				    </c:forEach>
			  	</select><br/>
				<label for="nbStagiaireMax">Nombre de stagiaires inscrits:</label>
				<input type="number" value=${session.nbStagiaire} id="nbStagiaire" name="nbStagiaire"/><br/>
				<label for="typeFormation">Formation inter/intra :</label>
			  	<select id="typeFormation" name="typeFormation">
				    <option value="inter" ${session.typeFormation == "inter" ? 'selected = "selected"' : ''}>Inter-Entreprise</option>
				    <option value="intra" ${session.typeFormation == "intra" ? 'selected = "selected"' : ''}>Intra-Entreprise</option>
			  	</select><br/>
		  		<label for="salle">Salle de formation :</label>
			  	<select id="salle" name="salleId">
				  	<c:forEach items="${salles}" var="salle">
				    	<option value="${salle.id}" ${salle.id == session.salle.id ? 'selected = "selected"' : ''}>${salle.nom}</option>
				    </c:forEach>
			  	</select><br/>
			  	<label for="responsable">Responsable de la session :</label>
			  	<select id="responsable" name="responsableId">
				    <c:forEach items="${responsables}" var="responsable">
				    	<option value="${responsable.id}" ${responsable.id == session.responsable.id ? 'selected = "selected"' : ''}>${responsable.email}</option>
				    </c:forEach>
			  	</select><br/>
			  	<input type="hidden" value="${param.id}" name="id"/>
		  		<input id="submitAjouterModifier" type="submit" value="Modifier la session">
			</form>
		</div>
		<p>* : Champs obligatoires</p>
		<a id="retour" href="sessionListe">Retour</a>
	</div>
</main>
</div>

<%@ include file="../footer.jspf" %> 
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>       
</body>
</html>