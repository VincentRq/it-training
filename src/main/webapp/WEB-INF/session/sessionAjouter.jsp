<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>IT-Training - Créer une session</title>
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageAjouterModifier.css">      
</head>
<body>
<%@ include file="../header.jspf" %>

<div id="container">
<main>
	<div class="body" id="firstBody">
		<h1>Prévoir une session</h1>
		<div id="form">
			<form action="sessionAjouter" method="post">
				<label for="nom">Intitulé de la session</label><br/>
				<select id="catalogue" name="catalogueId">
					<c:forEach items="${catalogues}" var="catalogue">
				    	<option value="${catalogue.id}">${catalogue.domaine} - ${catalogue.titre}</option>
				    </c:forEach>
				</select><br/>
				<label for="dateDebut">Date de début</label><br/>
				<input type="date" id="dateDebut" name="dateDebut"/><br/>
				<label for="dateFin">Date de fin</label><br/>
				<input type="date" id="dateFin" name="dateFin"/><br/>
				<label for="formateur">Formateur proposé</label><br/>
				<select id="formateur" name="formateurId">
					<c:forEach items="${formateurs}" var="formateur">
				    	<option value="${formateur.id}">${formateur.nom} ${formateur.prenom}</option>
				    </c:forEach>
				</select><br/>
			  	<label for="nbStagiaire">Nombre de stagiaire</label><br/>
				<input type="number" id="nbStagiaire" name="nbStagiaire" value="0"/><br/>
				<label for="typeFormation">Formation inter/intra</label><br/>
			  	<select id="typeFormation" name="typeFormation">
				    <option value="inter">Inter-Entreprise</option>
				    <option value="intra">Intra-Entreprise</option>
			  	</select><br/>
			  	<label for="salle">Salle de formation</label><br/>
				<select id="salle" name="salleId">
				  	<c:forEach items="${salles}" var="salle">
				    	<option value="${salle.id}">${salle.nom}</option>
				    </c:forEach>
				</select><br/>
				<label for="responsable">Responsable de la session</label><br/>
				<select id="responsable" name="responsableId">
				    <c:forEach items="${responsables}" var="responsable">
				    	<option value="${responsable.id}">${responsable.email}</option>
				    </c:forEach>
				</select><br/>
			  	<input id="submitAjouterModifier" type="submit" value="Créer la session">
			</form>
		</div>
		<p>* : Champs obligatoires</p>
		<a id="retour" href="sessionListe">Retour</a>
	</div>
</main>
</div>

<%@ include file="../footer.jspf" %> 
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>       
</body>
</html>