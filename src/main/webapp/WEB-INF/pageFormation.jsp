<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>IT-Training</title>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
<link rel="stylesheet" href="css/HeaderBis.css">
<link rel="stylesheet" href="css/Footer.css">
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
<link rel ="stylesheet" href="css/PageFormation.css">
          
</head>
<body>
<%@ include file="header.jspf" %>
	<div id="container">
		<main>
			<div class="body" id="firstBody">
				<c:choose>
				  <c:when test="${param.codeErreur == 1}">Accès à la page interdit. Vous devez être connecté pour pouvoir y accéder.</c:when>
				  <c:when test="${param.codeErreur == 2}">Accès à la page interdit. Vous devez être connecté en tant que formateur pour pouvoir y accéder.</c:when>
				  <c:when test="${param.codeErreur == 3}">Accès à la page interdit. Vous devez posséder les droits d'administration pour pouvoir y accéder.</c:when>
				  <c:otherwise></c:otherwise>
				</c:choose>
			    <div class="contenu" id="colonne1">
			        <article>
			            <h1>${catalogue.titre}</h1>
			            <h2>${catalogue.sousTheme}</h2><br>
			            <h3>Description</h3>
			            <p>${catalogue.description}</p>
			        </article>
			        <article><br>
			            <h3>Pré-requis</h3>
			            <p>${catalogue.prerequis}</p>
			            <br>
			            <p><a href="#">Effectuer le test de pré-requis</a></p>
			        </article>
		        	<article><br>
			            <h3>Programme</h3>
			            <p>${catalogue.programme}</p>
			            <br>
			            <p><a href="#">Télécharger le programme complet</a></p>
		        	</article>
	    		</div>
    		
	        <div class="contenu" id="colonne2">
	            <table id="formation">
	                <thead>
	                    <tr>
	                        <th colspan="2">Formation</th>
	                    </tr>
	                </thead>
	                <tbody>
	                    <tr>
	                    	<td class="colonneG">Titre</td>
	                        <td colspan="2">${catalogue.titre}</td>
	                    </tr>
	                	<tr>
		                    <td class="colonneG">Durée</td>
		                    <td>${catalogue.dureeH} H</td>
	                	</tr>
		                <tr>
		                    <td class="colonneG">Tarif (€)</td>
		                    <td><fmt:formatNumber type="number" minFractionDigits="0" maxFractionDigits="2" value="${catalogue.prix}" /></td>
		                </tr>
	                <tbody/>
	            </table>
		        <div id="boutons">
			         <form method="GET" action="${sessionScope.utilisateur != null ? 'inscriptionStagiaireSession' : 'connexionInscription'}">
			            <input class="bouton" type="submit" value="Inscription"/>
			          </form>
			          <form method="GET" action="contact"> 
			            <input class="bouton" type="submit" value="Contact"/>
			          </form>
		        </div>
		      </div>
	            <!--  <table id="prochaineSession">
	                <thead>
		                <tr>
		                    <th colspan="2">Prochaines sessions</th>
		                </tr>
	                </thead>
	                <tbody>
		                <tr>
		                    <td>Date</td>
		                    <td>???</td>
		                </tr>
		                <tr>
		                    <td>Tarif</td>
		                    <td>Nous contacter</td>
		                </tr>
	                <tbody>	                
	            </table> -->
        	</div>
	    </main>
	</div>

<%@ include file="footer.jspf" %> 
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>       
</body>
</html>