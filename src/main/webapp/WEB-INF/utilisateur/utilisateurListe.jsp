<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste des utilisateurs</title>
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageListe.css">
</head>
<%@ include file="../header.jspf" %>
<body>
<div id="container">
<main>
	<div class="body" id="firstBody">
		<a id="addNew" href="utilisateurAjouterModifier"><span id="link">Ajouter un nouvel utilisateur<span id="plus">+</span></span></a>
		<section id="utilisateurListe">
			<h1>Liste des utilisateurs</h1>
			<table>
				<thead>
					<tr>
						<th>Email</th>
						<th>Entreprise</th>
						<th>Statut</th>
						<th>Détails</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${utilisateurs}" var="utilisateur">
						<tr>
							<td>${utilisateur.email}</td>
							<td>${utilisateur.entreprise}</td>
							<td>
								<c:choose>
									<c:when test="${utilisateur.admin}">Admin</c:when>
									<c:otherwise>Utilisateur</c:otherwise>
								</c:choose>
							</td>
							<td>
								<form class="boutonDetail" action="utilisateurDetail" method="post">
									<input type="hidden" value="${utilisateur.id}" name="id"/>
									<input class="submit" type="submit" value="Voir l'utilisateur"/>
								</form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</section>
		<br/>
		<a class="bouton" id="retour" href="tableauDeBordAdmin">Retour au tableau de bord</a>
	</div>
</main>
</div>
<%@ include file="../footer.jspf" %> 
</body>
</html>