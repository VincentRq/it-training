<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageDetail.css">
<meta charset="UTF-8">
<title>Détails utilisateur</title>
</head>
<body>
<%@ include file="../header.jspf" %> 
<div id="container">
<main>
	<div class="body" id="firstBody">
		<h1>Utilisateur ${utilisateur.email}</h1>
		<table>
			<tr>
				<th>Entreprise :</th>
				<td>${utilisateur.entreprise != '' ? utilisateur.entreprise : 'Non renseignée'}</td>
			</tr>
			<tr>
				<th>Statut :</th>
				<td>
					<c:choose>
						<c:when test="${utilisateur.admin}">Admin</c:when>
						<c:otherwise>Aucun statut pour le moment</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<td>
					<c:choose>
						<c:when test="${utilisateur.admin}">
							<form class="boutonDetail" action="utilisateurDestituerAdmin" method="post">
								<input type="hidden" value="${utilisateur.id}" name="id"/>
								<input class="submit submitSupprimer" type="submit" value="Enlever les droits d'administrateur"/>
							</form>
						</c:when>
						<c:otherwise>
							<form class="boutonDetail" action="utilisateurNommerAdmin" method="post">
								<input type="hidden" value="${utilisateur.id}" name="id"/>
								<input class="submit submitCreer" type="submit" value="Nommer administrateur"/>
							</form>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<td>
					<c:choose>
						<c:when test="stagiaireExists">
							Stagiaire existant
						</c:when>
						<c:otherwise>
							<form class="boutonDetail" action="stagiaireAjouter" method="post">
								<input type="hidden" value="${utilisateur.id}" name="utilisateurId"/>
								<input class="submit submitCreer" type="submit" value="Créer profil stagiaire"/>
							</form>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
		</table>
		<div class="actions">
			<form action="utilisateurAjouterModifier" method="post">
				<input type="hidden" name="id" value="${utilisateur.id}"/>
				<input type="hidden" name="email" value="${utilisateur.email}"/>
				<input type="hidden" name="entreprise" value="${utilisateur.entreprise}"/>
				<input type="hidden" name="mdp" value="${utilisateur.mdp}"/>
				<input type="hidden" name="admin" value="${utilisateur.admin}"/>
				<input class="submitModifier" type="submit" value="Modifier l'utilisateur"/>
			</form>
			<form action="utilisateurSupprimer" method="post">
				<input type="hidden" name="id" value="${utilisateur.id}"/>
				<input class="submitSupprimer" type="submit" value="Supprimer l'utilisateur"/>
			</form>
		</div>
		<a id="retour" href="utilisateurListe">Retour</a>
	</div>
</main>
</div>

</body>
<%@ include file="../footer.jspf" %> 
</html>