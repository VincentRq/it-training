<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageAjouterModifier.css">
<title>Ajouter / modifier un utilisateur</title>
</head>
<body>
<%@ include file="../header.jspf" %>
<div id="container">
<main>
	<div class="body" id="firstBody">
		<h1>${param.id == null ? "Création d'un utilisateur" : "Modification de l'utilisateur"}</h1>
		<div id="form">
			<form action="utilisateurAjouterModifier" method="post">
				<label for="email">Email *</label><br/>
				<input type="text" id="email" name="email" value="${param.email}"/>${erreurs.get("email")}<br/>
				<label for="mdp">Mot de passe *</label><br/>
				<input type="password" id="mdp" name="mdp"/>${erreurs.get("mdp")}<br/>
				<label for="verifMdp">Confirmation du mot de passe *</label><br/>
				<input type="password" id="verifMdp" name="verifMdp"/>${erreurs.get("verifMdp")}<br/>
				<label for="entreprise">Entreprise</label><br/>
				<input type="text" id="entreprise" name="entreprise" value="${param.entreprise}"/>${erreurs.get("entreprise")}<br/>
  				<div id="booleenAdmin">
	  				<label for="admin">Admin :</label>
	  				<input type="radio" id="admin" name="admin" value="true">
	  				<label for="nonAdmin">Non admin :</label>
	  				<input type="radio" id="nonAdmin" name="admin" value="false" checked>
  				</div>
				<c:if test="${param.id != null}">
					<input type="hidden" value="${param.id}" name="id"/>
				</c:if>
				<input type="hidden" value="true" name="formulairePasse"/>
				<input id="submitAjouterModifier" type="submit" value="${param.id == null ? 'Créer l utilisateur' : 'Modifier l utilisateur'}"/>
			</form>
		</div>
		<p>* : Champs obligatoires</p>
		<a id="retour" href="utilisateurListe">Retour</a>
	</div>
</main>
</div>
<%@ include file="../footer.jspf" %>
</body>
</html>