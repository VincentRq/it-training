<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>IT-Training</title>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
<link rel="stylesheet" href="css/InscriptionStagiaireSession.css">

</head>
<body>

<%@ include file="header.jspf" %>

<div id="container">
<main>
	<div class="body" id="firstBody">
		<h2>Pour vous inscrire à une formation, merci de compléter les informations suivantes : </h2>
		<br/>
		<p class="erreur">${messageSuccess}</p>
		<br/>		
		
		<div class="form">
		
		    <form class="formulaire" method="POST" action="inscriptionStagiaireSession">
		        <h1>Informations personnelles</h1>
		        <br/>
		        <c:choose>
		        	<c:when test="${stagiaire == null}">
				    	<div id="formulaire">
					        <label>Nom *</label>
					        <input id="nom" class="input" type="text" name="nom" required maxlength="200"/>
					        <br/> <br>
					        <label>Prénom *</label>
					        <input id="prenom" class="input" type="text" name="prenom" required maxlength="200"/>
					        <br/> <br>
					        <label>Adresse postale *</label>
					        <input id="adressePostale" class="input" type="text" name="adressePostale" required maxlength="200"/>
					        <br/> <br>
					        <label>Code postal *</label>
					        <input id="codePostal" class="input" type="text" name="codePostal" required maxlength="20"/>
					        <br/> <br>
					        <label>Ville *</label>
					        <input id="ville" class="input" type="text" name="ville" required maxlength="200"/>
					        <br/> <br>
					        <label>Pays *</label>
					        <input id="pays" class="input" type="text" name="pays" required maxlength="200"/>
					        <br/> <br>
					        <p>* Champs obligatoires</p><br/>
				    	</div>
		        	</c:when>
		        	<c:otherwise>
				    	<div id="informations">
				        	<p>Nom : ${stagiaire.nom}</p>
				        	<p>Prénom : ${stagiaire.prenom}</p>
				        	<p>Adresse postale : ${stagiaire.adressePostale}</p>
				        	<p>Code postal : ${stagiaire.codePostal}</p>
				        	<p>Ville : ${stagiaire.ville}</p>
				        	<p>Pays : ${stagiaire.pays}</p>
				        	<p>id : ${stagiaire.id}</p>
				        	<input type="hidden" name="idStagiaire" value="${stagiaire.id}">
				    	</div>
		        	</c:otherwise>
		        </c:choose>
		        <br/>
		        
		        <h1>Choix de la session de formation</h1>
		        <br/>
		        <p>Formation : ${catalogue.titre}</p>
		        <br> <br>
		        <p>Prix : ${catalogue.prix} €</p>
		        <br> <br>
		        <label>Date de début de la session</label>
		        <select id="session" name="sessionId">
			        <br/>
				    <c:forEach items="${sessions}" var="session">
				    	<option value="${session.id}">${(session.dateDebutFR)}</option>
				    </c:forEach>
		       </select>
		      	<br><br><br><br>
		        <input class="bouton" type="submit" value="Demander mon inscription à cette session" name="inscriptionStagiaireSession"/>
		      </form>
		     </div>
		</div>
</main>
</div>

	
<%@ include file="footer.jspf" %> 
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>          
</body>
</html>