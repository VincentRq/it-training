<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageDetail.css">
<meta charset="UTF-8">
<title>Détails formateur</title>
</head>
<body>
<%@ include file="../header.jspf" %> 
<div id="container">
<main>
	<div class="body" id="firstBody">
		<h1>${formateur.prenom} ${formateur.nom}</h1>
		<table>
			<tr>
				<th>Date de recrutement :</th>
				<td>${formateur.dateRecrutement}</td>
			</tr>
			<tr>
				<th>Note :</th>
				<td>${formateur.note}</td>
			</tr>
			<tr>
				<th>Adresse email :</th>
				<td>${formateur.email}</td>
			</tr>
			<tr>
				<th>Adresse :</th>
				<td>${formateur.adresse}</td>
			</tr>
			<tr>
				<th>Code postal :</th>
				<td>${formateur.codePostal}</td>
			</tr>
			<tr>
				<th>Ville :</th>
				<td>${formateur.ville}</td>
			</tr>
			<tr>
				<th>Pays :</th>
				<td>${formateur.pays}</td>
			</tr>
			<tr>
				<th>Nombre de blame :</th>
				<td>${formateur.blame}</td>
			</tr>
		</table>
		<div class="actions">
			<form action="formateurAjouterModifier" method="post">
				<input type="hidden" name="id" value="${formateur.id}"/>
				<input type="hidden" name="prenom" value="${formateur.prenom}"/>
				<input type="hidden" name="nom" value="${formateur.nom}"/>
				<input type="hidden" name="dateRecrutement" value="${formateur.dateRecrutement}"/>
				<input type="hidden" name="note" value="${formateur.note}"/>
				<input type="hidden" name="email" value="${formateur.email}"/>
				<input type="hidden" name="adresse" value="${formateur.adresse}"/>
				<input type="hidden" name="codePostal" value="${formateur.codePostal}"/>
				<input type="hidden" name="ville" value="${formateur.ville}"/>
				<input type="hidden" name="pays" value="${formateur.pays}"/>
				<input type="hidden" name="blame" value="${formateur.blame}"/>
				<input class="submitModifier" type="submit" value="Modifier les informations du formateur"/>
			</form>
			<form action="formateurSupprimer" method="post">
				<input type="hidden" name="id" value="${formateur.id}"/>
				<input class="submitSupprimer" type="submit" value="Supprimer le formateur"/>
			</form>
		</div>
		<a id="retour" href="formateurListe">Retour</a>
	</div>
</main>
</div>

</body>
<%@ include file="../footer.jspf" %> 
</html>