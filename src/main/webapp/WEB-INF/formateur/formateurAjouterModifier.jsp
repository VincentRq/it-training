<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageAjouterModifier.css">
<title>Ajouter ou modifier un formateur</title>
</head>
<body>
<%@ include file="../header.jspf" %>
<div id="container">
<main>
	<div class="body" id="firstBody">
		<h1>${param.id == null ? 'Ajouter un nouveau formateur' : 'Modification du formateur'}</h1>
		<div id="form">
			<form action="formateurAjouterModifier" method="post">
				<label for="nom">Nom :</label>
				<input type="text" id="nom" name="nom" value="${param.nom}"/>${erreurs.get("nom")}<br/>
				<label for="prenom">Prenom :</label>
				<input type="text" id="prenom" name="prenom" value="${param.prenom}"/>${erreurs.get("prenom")}<br/>
				<label for="dateRecrutement">Date de recrutement :</label>
				<input type="date" id="dateRecrutement" name="dateRecrutement" value="${param.dateRecrutement}"/>${erreurs.get("dateRecrutement")}<br/>
				<label for="email">Email :</label>
				<input type="email" id="email" name="email" value="${param.email}"/>${erreurs.get("email")}<br/>
				<label for="adresse">Adresse postal :</label>
				<input type="text" id="adresse" name="adresse" value="${param.adresse}"/>${erreurs.get("adresse")}<br/>
				<label for="codePostal">Code postal :</label>
				<input type="text" id="codePostal" name="codePostal" value="${param.codePostal}"/>${erreurs.get("codePostal")}<br/>
				<label for="ville">Ville :</label>
				<input type="text" id="ville" name="ville" value="${param.ville}"/>${erreurs.get("ville")}<br/>
				<label for="pays">Pays :</label>
				<input type="text" id="pays" name="pays" value="${param.pays}"/>${erreurs.get("pays")}<br/>
				<label for="blame">Blame :</label>
				<input type="numeric" id="blame" name="blame" value="${param.blame == null ? 0 : param.blame}"/>${erreurs.get("blame")}<br/>
				<c:if test="${param.id != null}">
					<input type="hidden" value="${param.id}" name="id"/>
				</c:if>
				<input type="hidden" value="true" name="formulairePasse"/>
				<input id="submitAjouterModifier" type="submit" value="${param.id == null ? 'Créer le formateur' : 'Modifier le formateur'}"/>
			</form>
		</div>
		<p>* : Champs obligatoires</p>
		<a id="retour" href="formateurListe">Retour</a>
	</div>
</main>
</div>
<%@ include file="../footer.jspf" %>
</body>
</html>