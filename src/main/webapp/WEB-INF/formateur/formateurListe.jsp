<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste des formateurs</title>
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageListe.css">
</head>
<%@ include file="../header.jspf" %>
<body>
<div id="container">
<main>
	<div class="body" id="firstBody">
		<a id="addNew" href="formateurAjouterModifier"><span id="link">Ajouter un nouveau formateur<span id="plus">+</span></span></a>
		<section id="formateurs">
			<h1>Liste des formateurs</h1>
			<table>
				<thead>
					<tr>
						<th>Nom</th>
						<th>Prénom</th>
						<th>Date de recrutement</th>
						<th>Note</th>
						<th>Email</th>
						<th>Adresse</th>
						<th>Code postale</th>
						<th>Ville</th>
						<th>Pays</th>
						<th>Nombre de blame</th>
						<th>Détails</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${formateurs}" var="formateur">
						<tr>
							<td>${formateur.nom}</td>
							<td>${formateur.prenom}</td>
							<td>${formateur.dateRecrutement}</td>
							<td>${formateur.note}</td>
							<td>${formateur.email}</td>
							<td>${formateur.adresse}</td>
							<td>${formateur.codePostal}</td>
							<td>${formateur.ville}</td>
							<td>${formateur.pays}</td>
							<td>${formateur.blame}</td>
							<td>
								<form action="formateurDetail" method="post">
									<input type="hidden" value="${formateur.id}" name="formateurId"/>
									<input class="submit" type="submit" value="Détails du formateur"/>
								</form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</section>
		<br/>
		<a class="bouton" id="retour" href="tableauDeBordAdmin">Retour au tableau de bord</a>
	</div>
</main>
</div>
<%@ include file="../footer.jspf" %> 
</body>
</html>