<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>IT-Training</title>
<link rel ="stylesheet" href="css/Base.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
<link rel ="stylesheet" href="css/MesFormations.css">
          
</head>
<body>
<%@ include file="header.jspf" %>

<div id="container">
<main>

	<div class="body" id="firstBody">
	
	<c:choose>
	  <c:when test="${param.codeErreur == 1}">Accès à la page interdit. Vous devez être connecté pour pouvoir y accéder.</c:when>
	  <c:when test="${param.codeErreur == 2}">Accès à la page interdit. Vous devez être connecté en tant que formateur pour pouvoir y accéder.</c:when>
	  <c:when test="${param.codeErreur == 3}">Accès à la page interdit. Vous devez posséder les droits d'administration pour pouvoir y accéder.</c:when>
	  <c:otherwise></c:otherwise>
	</c:choose>
	
		<h1>Mes informations personnelles</H1>
<p>Nom :</p>
<p>Prénom :</p>
<p>Entreprise :</p>
<p>E-mail : ${utilisateur.email}</p>
<p>Adresse postale :</p>
<p>Téléphone :</p>

    <h1>Mes formations</H1>
    <table>
        <thead>
            <tr>
                <th id="Mesformations" colspan="5">Mes formations</th>
            </tr>
            <tr>
                <th>Formation</th>
                <th>Date de début</th>
                <th>Date de fin</th>
                <th>Salle</th>
                <th>Statut</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>CSS</td>
                <td>24/01/2022</td>
                <td>28/01/2022</td>
                <td>Cancale (Ib Formation)</td>
                <td>Terminée</td>
            </tr>
            <tr>
                <td>JAVA avancé</td>
                <td>31/01/2022</td>
                <td>04/02/2022</td>
                <td>Cancale (Ib Formation)</td>
                <td>En cours</td>
            </tr>
            <tr>
                <td>Javascript</td>
                <td>07/02/2022</td>
                <td>13/02/2022</td>
                <td>Horizon Software</td>
                <td>Confirmée</td>
            </tr>
    </table>

    <p>Rappel : Vous pouvez annuler votre inscription à une formation gratuitement jusqu'à 15 jours avant le début de la session. En dessous de ce délai, le réglement des frais de formation vous sera demandé. </p>
		
	</div>
</main>
</div>

<%@ include file="footer.jspf" %> 
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>       
</body>
</html>