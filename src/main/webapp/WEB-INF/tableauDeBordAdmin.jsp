<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Tableau de bord</title>
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/TableauDeBordAdmin.css">
</head>
<%@ include file="header.jspf" %>
<body>
<div id="container">
<main>
	<div class="body" id="firstBody">
		<div id="haut">
			<h1>Bienvenue sur ton tableau de bord !</h1>
			<p>Version admin</p>
			<br/>
			<div>
				<a class="bouton" href="deconnexion">Se déconnecter</a>
			</div>
		</div>
		<br/>
		<h2>Prochaines sessions</h2>
		<div id="corps">
			<div id="prochainesSessions">
				<section id="sessions">
				<c:forEach items="${sessions}" var="session">
					<article class="session">
						<form action="sessionDetail" method="post">
							<button value="${session.id}" name="sessionId">
								<h3>${session.catalogue.titre}</h3>
								<p>${session.dateDebutFR}</p>
								<p>Formateur : ${session.formateur.nom} ${session.formateur.prenom}</p>
								<p>Responsable : ${session.responsable.email}</p>
							</button>
						</form>
					</article>
				</c:forEach>
				</section>
			</div>
			<div id="listes">
				<h3>Gestion administrateur</h3>
				<ul>
					<li><a href="utilisateurListe">Utilisateurs</a></li>
					<li><a href="stagiaireListe">Stagiaire</a></li>
					<li><a href="formateurListe">Formateurs</a></li>
					<li><a href="catalogueListe">Catalogue de formations</a></li>
					<li><a href="sessionListe">Sessions de formation</a></li>
					<li><a href="salleListe">Salles</a></li>
				</ul>
			</div>
		</div>
	</div>
</main>
</div>
<%@ include file="footer.jspf" %> 
</body>
</html>