<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Qui sommes-nous ?</title>
<link rel ="stylesheet" href="css/Base.css">
          
</head>
<body>
<%@ include file="header.jspf" %>

<div id="container">
<main>
	<div class="body" id="firstBody">
		<h1>Qui sommes-nous ?</h1>
		<h2>Création</h2>
		<p>Créé en 2022, IT-Training est une filiale d'IB formation.
		Dès sa création à Paris, en 1985, ib s'est positionnée sur le marché de la formation informatique en proposant une 
		offre de formations informatiques à forte valeur ajoutée. En 1999, ib devient filiale du Groupe Cegos, leader européen 
		du management par les compétences. Les synergies qui en découleront permettront notamment à ib de renforcer son 
		savoir-faire pédagogique et ses capacités d'innovation.
		Aujourd'hui, ib - groupe Cegos, implantée dans 11 villes en France, accompagne les entreprises dans la définition et la 
		mise en oeuvre de leurs projets informatiques. ib leur propose pour cela un large éventail de solutions de formation et 
		d'accompagnement afin d'apporter des réponses adaptées à chacune de leurs problématiques, qu'elles soient d'ordre 
		technique ou humain.
		A travers son offre de 1 150 séminaires et formations aux métiers et technologies de l’informatique, ib assure chaque 
		année la montée en compétences de plus de 25 000 spécialistes des technologies de l’information.</p>
		<br/>
		<h2>Nos objectifs</h2>
		<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laborum maiores sit sapiente laudantium reprehenderit 
		provident, cum exercitationem eveniet voluptatum rem tempora alias, quidem doloribus, quasi voluptas blanditiis facere 
		rerum dolorem?
		Harum ipsum, eligendi libero sapiente quae distinctio aliquid qui, molestias cumque incidunt quam voluptas voluptates 
		dolorem vitae. Nisi at nulla, ipsam perferendis quasi, a rem culpa laborum saepe, quia quibusdam.
		Praesentium pariatur saepe ipsa, libero omnis dolores aut sunt excepturi aliquid. Officia esse excepturi ex officiis 
		repellendus id sint, animi voluptate enim debitis sit minima placeat ratione labore dolore ipsam.
		Perspiciatis illo minima voluptatem accusamus hic sit sint consectetur maiores sed natus! Quidem doloribus impedit 
		dolorem, commodi modi, numquam accusamus id labore vitae pariatur soluta esse eum distinctio quia iure?
		Nesciunt minus quia harum earum nemo, odio unde tenetur fugit magnam quod exercitationem corporis totam ea repellendus 
		nobis molestias mollitia nisi, beatae illum dignissimos laboriosam numquam ullam quam! Aspernatur, libero.</p>
	</div>
</main>
</div>
<%@ include file="footer.jspf" %>          
</body>
</html>