<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>IT-Training</title>

<link rel="stylesheet" href="css/Accueil.css">
<link rel="stylesheet" href="css/Base.css">
       
</head>
<body>
<div id="header">
	<%@ include file="header.jspf" %>
</div>

<div id="container">
<main>
	
	<div class="body" id="firstBody">
		<p id="titre">Bienvenue sur le portail de formation IT Training.</p>
		<br/>
	
		<c:choose>
	  <c:when test="${param.codeErreur == 1}">Accès à la page interdit. Vous devez être connecté pour pouvoir y accéder.</c:when>
	  <c:when test="${param.codeErreur == 2}">Accès à la page interdit. Vous devez être connecté en tant que formateur pour pouvoir y accéder.</c:when>
	  <c:when test="${param.codeErreur == 3}">Accès à la page interdit. Vous devez posséder les droits d'administration pour pouvoir y accéder.</c:when>
	  <c:otherwise></c:otherwise>
		</c:choose>
		
		<c:if test="${sessionScope.utilisateur != null}">
			<p>Vous êtes connecté avec le mail ${sessionScope.utilisateur.email}</p>
		</c:if>
		<br/>
		

		<div class="actualite">
			<a href="https://www.francecompetences.fr/recherche_certificationprofessionnelle/"><img class="certif" src="images/actualite.png" alt="actualite"></a>
			<div class="titreActualite">
				<div class="texteActualite">
					<p id="actualite">Actualité</p>
					<br/>
					<p>Obtenez une certification diplomante !</p>
					<br/>
					<p id="paragraphe">Les formations IT-Training évoluent pour vous fournir une certification reconnue par le Répertoire National des Certifications Professionnelles. Découvrez la lste des formations concernées. </p>
				</div>
			</div>
		</div>
			
		
		<h1>Nos Formations</h1>
		<br/>
		<h2><a href="formationInformatique">Formation en informatiques</a></h2>
		<p>Les formations populaires :</p>
		<br/>
					
			<div class="cartes" >
				<form method="POST" action="pageFormation">
					<div class="card" style="width: 18rem;">
						<input name="id" type="hidden" value="1"/>
						<button class="cardButton">
							<div class="card-body">
								<p class="card-text">Langage Python</p>
								<br/>
								<img class="image" src="images/logo_python_resized.png" class="card-img-top" alt="logo_python" title="Formation Python">
							</div>
						</button>
					</div>
				</form>
				<div class="card" style="width: 18rem;">
					<form method="POST" action="pageFormation">
						<div>
							<input name="id" type="hidden" value="2"/>
							<button class="cardButton">
								<div class="card-body">
									<p class="card-text">Langage Java</p>
									<br/>
									<img class="image" src="images/logo_java_resized.png" class="card-img-top" alt="logo_java" title="Formation Java">
								</div>
							</button>
						</div>
					</form>
				</div>
				<div class="card" style="width: 18rem;">
					<form method="POST" action="pageFormation">
						<input name="id" type="hidden" value="3"/>
						<button class="cardButton">
							<div class="card-body">
								<p class="card-text">Langage PHP</p>
								<br/>
								<img class="image" src="images/logo_php_resized.png" class="card-img-top" alt="logo_php" title="Formation PHP">
							</div>
						</button>
					</form>
				</div>
			</div>			
		
		<br/>
		<p>Nos formations en informatique proposent d'apprendre le développement d'applications web à travers l'utilisation de différents langages de programmation.</p>
		<br/>
		<p><a href="formationInformatique">► Découvrir toutes les formations en informatiques</a></p>
		<br/>
	
		<h2><a href="formationManagement">Formations management</a></h2>
		<p>Les formations populaires :</p>
		<br/>
		
		<div class="cartes">	
			<div class="card" style="width: 18rem;">
				<div class="card-body">
					<br/>
					<p class="card-text">Collaborer</p>
					<br/>
					<img class="image" src="images/management1_resized.jpg" class="card-img-top" alt="logo_management1" title="Formation Management 1">
				</div>
			</div>
			<div class="card" style="width: 18rem;">
				<div class="card-body">
					<br/>
					<p class="card-text">Créer un esprit d'équipe</p>
					<br/>
					<img class="image" src="images/management2_resized.jpg" class="card-img-top" alt="logo_management2" title="Formation Management 2">
				</div>
			</div>
			<div class="card" style="width: 18rem;">
				<div class="card-body">
					<br/>
					<p class="card-text">Gérer les conflits</p>
					<br/>
					<img class="image" src="images/management3_resized.jpg" class="card-img-top" alt="logo_management3" title="Formation Management 3">
				</div>
			</div>
		</div>
		<br/>
		<p>Nos formations au management proposent un programme complet pour gérer efficacement une équipe et l'aider à atteindre ses objectifs.</p>
		<br/>
		<p><a href="formationManagement">► Découvrir toutes les formations de management</a></p>
		<br/>
	
		<h2><a href="formationRessourcesHumaines">Formations ressources humaines</a></h2>
		<p>Les formations populaires :</p>
		<br/>
		
		<div class="cartes">	
			<div class="card" style="width: 18rem;">
				<div class="card-body">
					<br/>
					<p class="card-text">SIRH</p>
					<br/>
					<img class="image" src="images/sirh_resized.jpg" class="card-img-top" alt="logo_sirh" title="Formation SIRH">
				</div>
			</div>
			<div class="card" style="width: 18rem;">
				<div class="card-body">
					<br/>
					<p class="card-text">Ressources humaines</p>
					<br/>
					<img class="image" src="images/HR_resized.jpg" class="card-img-top" alt="logo_hr" title="Formation RH 1">
				</div>
			</div>
			<div class="card" style="width: 18rem;">
				<div class="card-body">
					<br/>
					<p class="card-text">Gestionnaire</p>
					<br/>
					<img class="image" src="images/RH_resized.jpg" class="card-img-top" alt="logo_rh" title="Formation RH 2">
				</div>
			</div>
		</div>
		<br/>
		<p>Découvrez les nouvelles méthodes, comme les plus éprouvées, pour accompagner efficacement vos salariés.</p>
		<br/>
		<p><a href="formationRessourcesHumaines">► Découvrir toutes les formations ressources humaines</a></p>
		<br/>
		
		<h2><a href="formationBureautique">Formations bureautiques</a></h2>
		
		<p>Les formations populaires :</p>
		<br/>
		
		<div class="cartes">	
			<div class="card" style="width: 18rem;">
				<div class="card-body">
					<br/>
					<p class="card-text">Word</p>
					<br/>
					<img class="image" src="images/word_resized.jpg" class="card-img-top" alt="logo_word" title="Formation Word">
				</div>
			</div>
			<div class="card" style="width: 18rem;">
				<div class="card-body">
					<br/>
					<p class="card-text">Excel</p>
					<br/>
					<img class="image" src="images/excel_resized.jpg" class="card-img-top" alt="logo_excel" title="Formation Excel">
				</div>
			</div>
			<div class="card" style="width: 18rem;">
				<div class="card-body">
					<br/>
					<p class="card-text">Power Point</p>
					<br/>
					<img class="image" src="images/ppt_resized.png" class="card-img-top" alt="logo_ppt" title="Formation PPT">
				</div>
			</div>
		</div>
		
		
		<br/>
		<p>Devenez un expert de la suite Office en apprenant à optimiser le traitement de vos données.</p>
		<br/>
		<p><a href="formationBureautique">► Découvrir toutes les formations bureautiques</a></p><br/>
		
		<h2><a href="formationFinance">Formations finance</a></h2>
		<p>Les formations populaires :</p>
		<br/>
		
		<div class="cartes">	
			<div class="card" style="width: 18rem;">
				<div class="card-body">
					<br/>
					<p class="card-text">Contrôle de gestion</p>
					<br/>
					<img class="image" src="images/finance1_resized.jpg" class="card-img-top" alt="logo_finance1" title="Formation Finance 1">
				</div>
			</div>
			<div class="card" style="width: 18rem;">
				<div class="card-body">
					<br/>
					<p class="card-text">Pilotage de la performance</p>
					<br/>
					<img class="image" src="images/finance2_resized.jpg" class="card-img-top" alt="logo_finance2" title="Formation Finance 2">
				</div>
			</div>
			<div class="card" style="width: 18rem;">
				<div class="card-body">
					<br/>
					<p class="card-text">Premiers pas en bourse</p>
					<br/>
					<img class="image" src="images/finance3_resized.jpg" class="card-img-top" alt="logo_finance3" title="Formation Finance 3">
				</div>
			</div>
		</div>
		
		<br/>
		<p>Comment gérer vos finances ? Est-ce le bon moment pour investir en bourse ? Osez la performance grâce à nos formations dans le domaine de la finance.</p>
		<br/>
		<p><a href="formationFinance">► Découvrir toutes les formations finance</a></p>	
		
	</div>
</main>
</div>

<%@ include file="footer.jspf" %> 
      
</body>
</html>