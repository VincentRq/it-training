<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageDetail.css">
<meta charset="UTF-8">
<title>Détails catalogue</title>
</head>
<body>
<%@ include file="../header.jspf" %> 
<div id="container">
<main>
	<div class="body" id="firstBody">
		<h1>Catalogue ${catalogue.titre}</h1>
		<table>
			<tr>
				<th>Domaine :</th>
				<td>${catalogue.domaine}</td>
			</tr>
			<tr>
				<th>Thème :</th>
				<td>${catalogue.theme}</td>
			</tr>
			<tr>
				<th>Sous-thème :</th>
				<td>${catalogue.sousTheme}</td>
			</tr>
			<tr>
				<th>Durée (en heures) :</th>
				<td>${catalogue.dureeH}</td>
			</tr>
			<tr>
				<th>Prix (€) :</th>
				<td>${catalogue.prix}</td>
			</tr>
			<tr>
				<th>Nombre de stagiaires minimum :</th>
				<td>${catalogue.nbStagiaireMin}</td>
			</tr>
			<tr>
				<th>Nombre de stagiaires maximum :</th>
				<td>${catalogue.nbStagiaireMax}</td>
			</tr>
			<tr>
				<th>Description :</th>
				<td>${catalogue.description}</td>
			</tr>
			<tr>
				<th>Programme :</th>
				<td>${catalogue.programme}</td>
			</tr>
			<tr>
				<th>Prérequis :</th>
				<td>${catalogue.prerequis}</td>
			</tr>
		</table>
		<div class="actions">
			<form action="catalogueAjouterModifier" method="post">
				<input type="hidden" name="id" value="${catalogue.id}"/>
				<input type="hidden" name="titre" value="${catalogue.titre}"/>
				<input type="hidden" name="domaine" value="${catalogue.domaine}"/>
				<input type="hidden" name="theme" value="${catalogue.theme}"/>
				<input type="hidden" name="sousTheme" value="${catalogue.sousTheme}"/>
				<input type="hidden" name="dureeH" value="${catalogue.dureeH}"/>
				<input type="hidden" name="prix" value="${catalogue.prix}"/>
				<input type="hidden" name="nbStagiaireMin" value="${catalogue.nbStagiaireMin}"/>
				<input type="hidden" name="nbStagiaireMax" value="${catalogue.nbStagiaireMax}"/>
				<input type="hidden" name="description" value="${catalogue.description}"/>
				<input type="hidden" name="programme" value="${catalogue.programme}"/>
				<input type="hidden" name="prerequis" value="${catalogue.prerequis}"/>
				<input class="submitModifier" type="submit" value="Modifier le catalogue"/>
			</form>
			<form action="catalogueSupprimer" method="post">
				<input type="hidden" name="id" value="${catalogue.id}"/>
				<input class="submitSupprimer" type="submit" value="Supprimer le catalogue"/>
			</form>
		</div>
		<a id="retour" href="catalogueListe">Retour</a>
	</div>
</main>
</div>

</body>
<%@ include file="../footer.jspf" %> 
</html>