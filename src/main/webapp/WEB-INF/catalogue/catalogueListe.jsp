<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste formations catalogue</title>
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageListe.css">
</head>
<%@ include file="../header.jspf" %>
<body>
<div id="container">
<main>
	<div class="body" id="firstBody">
		<a id="addNew" href="catalogueAjouterModifier"><span id="link">Ajouter une nouvelle formation au catalogue<span id="plus">+</span></span></a>
		<section id="catalogueListe">
			<h1>Liste des formations du catalogue</h1>
			<table>
				<thead>
					<tr>
						<th>Titre</th>
						<th>Domaine</th>
						<th>Thème</th>
						<th>Sous-thème</th>
						<th>Durée</th>
						<th>Prix (€)</th>
						<th>Détails</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${catalogues}" var="catalogue">
						<tr>
							<td>${catalogue.titre}</td>
							<td>${catalogue.domaine}</td>
							<td>${catalogue.theme}</td>
							<td>${catalogue.sousTheme}</td>
							<td>${catalogue.dureeH}</td>
							<td><fmt:formatNumber type="number" minFractionDigits="0" maxFractionDigits="2" value="${catalogue.prix}" /></td>
							<td>
								<form class="boutonDetail" action="catalogueDetail" method="post">
									<input type="hidden" value="${catalogue.id}" name="catalogueId"/>
									<input class="submit" type="submit" value="Voir la formation"/>
								</form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</section>
		<br/>
		<a class="bouton" id="retour" href="tableauDeBordAdmin">Retour au tableau de bord</a>
	</div>
</main>
</div>
<%@ include file="../footer.jspf" %> 
</body>
</html>