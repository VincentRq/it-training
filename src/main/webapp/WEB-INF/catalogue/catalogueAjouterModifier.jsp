<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageAjouterModifier.css">
<title>Ajouter un catalogue</title>
</head>
<body>
<%@ include file="../header.jspf" %>
<div id="container">
<main>
	<div class="body" id="firstBody">
		<h1>${param.id == null ? 'Ajouter une formation au catalogue' : 'Modification du catalogue de formation' }</h1>
		<div id="form">
			<form action="catalogueAjouterModifier" method="post">
				<label for="titre">Titre *</label>
				<input type="text" id="titre" name="titre" value="${param.titre}"/>${erreurs.get("titre")}<br/>
				<label for="domaine">Domaine *</label>
				<input type="text" id="domaine" name="domaine" value="${param.domaine}"/>${erreurs.get("domaine")}<br/>
				<label for="theme">Thème *</label>
				<input type="text" id="theme" name="theme" value="${param.theme}"/>${erreurs.get("theme")}<br/>
				<label for="sousTheme">Sous-thème</label>
				<input type="text" id="sousTheme" name="sousTheme" value="${param.sousTheme}"/>${erreurs.get("sousTheme")}<br/>
				<label for="dureeH">Durée (en heure)</label>
				<input type="number" id="dureeH" name="dureeH" value="${param.dureeH == null ? 10 : param.dureeH}"/>${erreurs.get("dureeH")}<br/>
				<label for="prix">Prix (€)</label>
				<input type="number" step="0.01" id="prix" name="prix" value="${param.prix}"/>${erreurs.get("prix")}<br/>
				<label for="nbStagiaireMin">Nombre de stagiaire minimum :</label>
				<input type="number" id="nbStagiaireMin" name="nbStagiaireMin" value="${param.nbStagiaireMin == null ? 3 : param.nbStagiaireMin}"/>${erreurs.get("nbStagiaireMin")}<br/>
				<label for="nbStagiaireMax">Nombre de stagiaire maximum :</label>
				<input type="number" id="nbStagiaireMax" name="nbStagiaireMax" value="${param.nbStagiaireMax == null ? 15 : param.nbStagiaireMax}"/>${erreurs.get("nbStagiaireMax")}<br/>
				<label for="description">Description :</label>
				<textarea name="description" id="description">${param.description}</textarea>${erreurs.get("description")}<br/>
				<label for="programme">Programme :</label>
				<textarea name="programme" id="programme">${param.programme}</textarea>${erreurs.get("programme")}<br/>
				<label for="prerequis">Prérequis :</label>
				<textarea name="prerequis" id="prerequis">${param.prerequis}</textarea>${erreurs.get("prerequis")}<br/>
				<c:if test="${param.id != null}">
					<input type="hidden" value="${param.id}" name="id"/>
				</c:if>
				<input type="hidden" value="true" name="formulairePasse"/>
				<input id="submitAjouterModifier" type="submit" value="${param.id == null ? 'Ajouter au catalogue de formation' : 'Modifier le catalogue'}"/>
			</form>
		</div>
		<p>* : Champs obligatoires</p>
		<a id="retour" href="catalogueListe">Retour</a>
	</div>
</main>
</div>
<%@ include file="../footer.jspf" %>
</body>
</html>