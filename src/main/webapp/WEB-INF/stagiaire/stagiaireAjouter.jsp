<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageAjouterModifier.css">
<title>Ajouter ou modifier un stagiaire</title>
</head>
<body>
<%@ include file="../header.jspf" %>
<div id="container">
<main>
	<div class="body" id="firstBody">
		<h1>${param.id == null ? 'Ajouter un nouveau stagiaire' : 'Modification du stagiaire'}</h1>
		<div id="form">
			<form action="stagiaireAjouter" method="post">
				<label for="utilisateur">Utilisateur :</label><br/>
				<c:choose>
					<c:when test="${utilisateur != null}">
						<p id="utilisateur">${utilisateur.email}</p>
						<input type="hidden" name="utilisateurId" value="${utilisateur.id}"/>
					</c:when>
					<c:otherwise>
						Salut !
						<select id="utilisateur" name="utilisateurId">
							<c:forEach items="${utilisateurs}" var="utilisateur">
						    	<option value="${utilisateur.id}">${utilisateur.email}</option>
						    </c:forEach>
						</select>
					</c:otherwise>
				</c:choose>
				<label for="nom">Nom :</label>
				<input type="text" id="nom" name="nom" value="${param.nom}"/>${erreurs.get("nom")}<br/>
				<label for="prenom">Prenom :</label>
				<input type="text" id="prenom" name="prenom" value="${param.prenom}"/>${erreurs.get("prenom")}<br/>
				<label for="adressePostale">Adresse postale :</label>
				<input type="text" id="adressePostale" name="adressePostale" value="${param.adressePostale}"/>${erreurs.get("adressePostale")}<br/>
				<label for="codePostal">Code postal :</label>
				<input type="text" id="codePostal" name="codePostal" value="${param.codePostal}"/>${erreurs.get("codePostal")}<br/>
				<label for="ville">Ville :</label>
				<input type="text" id="ville" name="ville" value="${param.ville}"/>${erreurs.get("ville")}<br/>
				<label for="pays">Pays :</label>
				<input type="text" id="pays" name="pays" value="${param.pays}"/>${erreurs.get("pays")}<br/>
				<c:if test="${param.id != null}">
					<input type="hidden" value="${param.id}" name="id"/>
				</c:if>
				<input type="hidden" value="true" name="formulairePasse"/>
				<input id="submitAjouterModifier" type="submit" value="${param.id == null ? 'Créer le stagiaire' : 'Modifier le stagiaire'}"/>
			</form>
		</div>
		<p>* : Champs obligatoires</p>
		<a id="retour" href="stagiaireListe">Retour</a>
	</div>
</main>
</div>
<%@ include file="../footer.jspf" %>
</body>
</html>