<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageDetail.css">
<meta charset="UTF-8">
<title>Détails stagiaire</title>
</head>
<body>
<%@ include file="../header.jspf" %> 
<div id="container">
<main>
	<div class="body" id="firstBody">
		<h1>${stagiaire.prenom} ${stagiaire.nom}</h1>
		<table>
			<tr>
				<th>Adresse électronique utilisateur :</th>
				<td>${utilisateurAssocie.email}</td>
			</tr>
			<tr>
				<th>Adresse :</th>
				<td>${stagiaire.adressePostale}</td>
			</tr>
			<tr>
				<th>Code postal :</th>
				<td>${stagiaire.codePostal}</td>
			</tr>
			<tr>
				<th>Ville :</th>
				<td>${stagiaire.ville}</td>
			</tr>
			<tr>
				<th>Pays :</th>
				<td>${stagiaire.pays}</td>
			</tr>
		</table>
		<div class="actions">
			<form action="stagiaireModifier" method="post">
				<input type="hidden" name="idUtilisateurAssocie" value="${utilisateurAssocie.id}"/>
				<input type="hidden" name="id" value="${stagiaire.id}"/>
				<input type="hidden" name="prenom" value="${stagiaire.prenom}"/>
				<input type="hidden" name="nom" value="${stagiaire.nom}"/>
				<input type="hidden" name="adressePostale" value="${stagiaire.adressePostale}"/>
				<input type="hidden" name="codePostal" value="${stagiaire.codePostal}"/>
				<input type="hidden" name="ville" value="${stagiaire.ville}"/>
				<input type="hidden" name="pays" value="${stagiaire.pays}"/>
				<input class="submitModifier" type="submit" value="Modifier les informations du stagiaire"/>
			</form>
			<form action="stagiaireSupprimer" method="post">
				<input type="hidden" name="id" value="${stagiaire.id}"/>
				<input class="submitSupprimer" type="submit" value="Supprimer le stagiaire"/>
			</form>
		</div>
		<a id="retour" href="stagiaireListe">Retour</a>
	</div>
</main>
</div>

</body>
<%@ include file="../footer.jspf" %> 
</html>