<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste des stagiaires</title>
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageListe.css">
</head>
<%@ include file="../header.jspf" %>
<body>
<div id="container">
<main>
	<div class="body" id="firstBody">
		<a id="addNew" href="stagiaireAjouter"><span id="link">Ajouter un nouveau stagiaire<span id="plus">+</span></span></a>
		<section id="stagiaires">
			<h1>Liste des stagiaires</h1>
			<table>
				<thead>
					<tr>
						<th>Nom</th>
						<th>Prénom</th>
						<th>Adresse postale</th>
						<th>Code postale</th>
						<th>Ville</th>
						<th>Pays</th>
						<th>Détails</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${stagiaires}" var="stagiaire">
						<tr>
							<td>${stagiaire.nom}</td>
							<td>${stagiaire.prenom}</td>
							<td>${stagiaire.adressePostale}</td>
							<td>${stagiaire.codePostal}</td>
							<td>${stagiaire.ville}</td>
							<td>${stagiaire.pays}</td>
							<td>
								<form action="stagiaireDetail" method="post">
									<input type="hidden" value="${stagiaire.id}" name="stagiaireId"/>
									<input class="submit" type="submit" value="Détails du stagiaire"/>
								</form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</section>
		<br/>
		<a class="bouton" id="retour" href="tableauDeBordAdmin">Retour au tableau de bord</a>
	</div>
</main>
</div>
<%@ include file="../footer.jspf" %> 
</body>
</html>