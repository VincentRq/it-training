<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Contact</title>
<link rel ="stylesheet" href="css/Base.css">
<link rel ="stylesheet" href="css/RechercheCatalogue.css">
          
</head>
<body>
<%@ include file="header.jspf" %>

<div id="container">
<main>
	<div class="body" id="firstBody">
		<h1>Les résultats de votre recherche : ${param.recherche}</h1>
		<c:choose>
			<c:when test="${cataloguesSelectionnes.isEmpty()}">
				<h2>Aucun résultat</h2>
			</c:when>
			<c:otherwise>
				<div id="catalogues">
					<c:forEach items="${catalogues}" var="catalogue">
						<article class="catalogue">
							<form action="pageFormation" method="post">
								<button name="id" value="${catalogue.id}">
									<h3>${catalogue.titre}</h3>
									<p>Domaine : ${catalogue.domaine}</p>
									<p>Thème : ${catalogue.theme}</p>
									<p>Sous-thème : ${catalogue.sousTheme}</p>
								</button>
							</form>
						</article>
					</c:forEach>
				</div>
			</c:otherwise>
		</c:choose>
	</div>
</main>
</div>
<%@ include file="footer.jspf" %>         
</body>
</html>