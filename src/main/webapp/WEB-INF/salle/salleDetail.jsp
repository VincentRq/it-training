<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageDetail.css">
<meta charset="UTF-8">
<title>Détails salle</title>
</head>
<body>
<%@ include file="../header.jspf" %> 
<div id="container">
<main>
	<div class="body" id="firstBody">
		<h1>Salle ${salle.nom}</h1>
		<table>
			<tr>
				<th>Capacité :</th>
				<td>${salle.capacite}</td>
			</tr>
			<tr>
				<th>Adresse :</th>
				<td>${salle.adresse}</td>
			</tr>
			<tr>
				<th>Code postal :</th>
				<td>${salle.codePostal}</td>
			</tr>
			<tr>
				<th>Ville :</th>
				<td>${salle.ville}</td>
			</tr>
			<tr>
				<th>Pays :</th>
				<td>${salle.pays}</td>
			</tr>
		</table>
		<div class="actions">
			<form action="salleAjouterModifier" method="post">
				<input type="hidden" name="id" value="${salle.id}"/>
				<input type="hidden" name="nom" value="${salle.nom}"/>
				<input type="hidden" name="capacite" value="${salle.capacite}"/>
				<input type="hidden" name="adresse" value="${salle.adresse}"/>
				<input type="hidden" name="codePostal" value="${salle.codePostal}"/>
				<input type="hidden" name="ville" value="${salle.ville}"/>
				<input type="hidden" name="pays" value="${salle.pays}"/>
				<input class="submitModifier" type="submit" value="Modifier la salle"/>
			</form>
			<form action="salleSupprimer" method="post">
				<input type="hidden" name="id" value="${salle.id}"/>
				<input class="submitSupprimer" type="submit" value="Supprimer la salle"/>
			</form>
		</div>
		<a id="retour" href="salleListe">Retour</a>
	</div>
</main>
</div>

</body>
<%@ include file="../footer.jspf" %> 
</html>