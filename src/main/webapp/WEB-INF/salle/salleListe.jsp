<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Liste des salles</title>
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageListe.css">
</head>
<%@ include file="../header.jspf" %>
<body>
<div id="container">
<main>
	<div class="body" id="firstBody">
		<a id="addNew" href="salleAjouterModifier"><span id="link">Ajouter une nouvelle salle<span id="plus">+</span></span></a>
		<section id="salleListe">
			<h1>Liste des salles</h1>
			<table>
				<thead>
					<tr>
						<th>Nom</th>
						<th>Capacité</th>
						<th>Adresse</th>
						<th>Code postale</th>
						<th>Ville</th>
						<th>Pays</th>
						<th>Détails</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${salles}" var="salle">
						<tr>
							<td>${salle.nom}</td>
							<td>${salle.capacite}</td>
							<td>${salle.adresse}</td>
							<td>${salle.codePostal}</td>
							<td>${salle.ville}</td>
							<td>${salle.pays}</td>
							<td>
								<form class="boutonDetail" action="salleDetail" method="post">
									<input type="hidden" value="${salle.id}" name="salleId"/>
									<input class="submit" type="submit" value="Voir la salle"/>
								</form>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</section>
		<br/>
		<a class="bouton" id="retour" href="tableauDeBordAdmin">Retour au tableau de bord</a>
	</div>
</main>
</div>
<%@ include file="../footer.jspf" %> 
</body>
</html>