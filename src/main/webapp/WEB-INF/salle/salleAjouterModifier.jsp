<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageAjouterModifier.css">
<title>Ajouter / modifier une salle</title>
</head>
<body>
<%@ include file="../header.jspf" %>
<div id="container">
<main>
	<div class="body" id="firstBody">
		<h1>${param.id == null ? 'Création de salle' : 'Modification de la salle'}</h1>
		<div id="form">
			<form action="salleAjouterModifier" method="post">
				<label for="nom">Nom *</label><br/>
				<input type="text" id="nom" name="nom" value="${param.nom}"/>${erreurs.get("nom")}<br/>
				<label for="capacite">Capacité (nombre d'élève) *</label><br/>
				<input type="number" id="capacite" name="capacite" value="${param.capacite == null ? 12 : param.capacite}"/>${erreurs.get("capacite")}<br/>
				<label for="adresse">Adresse postal</label><br/>
				<input type="text" id="adresse" name="adresse" value="${param.adresse}"/>${erreurs.get("adresse")}<br/>
				<label for="codePostal">Code postal</label><br/>
				<input type="text" id="codePostal" name="codePostal" value="${param.codePostal}"/>${erreurs.get("codePostal")}<br/>
				<label for="ville">Ville</label><br/>
				<input type="text" id="ville" name="ville" value="${param.ville}"/>${erreurs.get("ville")}<br/>
				<label for="pays">Pays</label><br/>
				<input type="text" id="pays" name="pays" value="${param.pays}"/>${erreurs.get("pays")}<br/>
				<c:if test="${param.id != null}">
					<input type="hidden" value="${param.id}" name="id"/>
				</c:if>
				<input type="hidden" value="true" name="formulairePasse"/>
				<input id="submitAjouterModifier" type="submit" value="${param.id == null ? 'Créer la salle' : 'Modifier la salle'}"/>
			</form>
		</div>
		<p>* : Champs obligatoires</p>
		<a id="retour" href="salleListe">Retour</a>
	</div>
</main>
</div>
<%@ include file="../footer.jspf" %>
</body>
</html>