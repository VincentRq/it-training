<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>IT-Training - Formations Finance</title>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
<link rel="stylesheet" href="css/FormationDomaine.css">
       
</head>
<body>
<div id="header">
	<%@ include file="../header.jspf" %>		<!-- utiliser headerBis.jspf plutot que header.jspf -->
</div>

<div id="container">
<main>
	
	<div class="body" id="firstBody">
		
		<h1>Nos formations en finance</h1>
		<br/>
		
		<p>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nisl eros, pulvinar facilisis justo mollis, auctor consequat urna. Morbi a bibendum metus.
		Donec scelerisque sollicitudin enim eu venenatis. Duis tincidunt laoreet ex, in pretium orci vestibulum eget. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis pharetra luctus lacus ut vestibulum. Maecenas ipsum lacus, lacinia quis posuere ut, pulvinar vitae dolor.
		Integer eu nibh at nisi ullamcorper sagittis id vel leo. Integer feugiat faucibus libero, at maximus nisl suscipit posuere. Morbi nec enim nunc. 
		Phasellus bibendum turpis ut ipsum egestas, sed sollicitudin elit convallis. 
		</p>	
		<br/>
				
		<c:forEach items="${themes}" var="theme">
			<h3>${theme}</h3>
			<c:forEach items="${catalogues}" var="catalogue">
				<c:if test="${ catalogue.getTheme().equals(theme)}">
					<form method="POST" action="pageFormation">
						<input name="id" type="hidden" value="${catalogue.getId()}"/>
						<p><button type="submit" class="link"><span>${catalogue.getTitre()}</span></button></p>
					</form>
				</c:if>	
			</c:forEach>
		</c:forEach>
		
		
	</div>
</main>
</div>

	
	

<%@ include file="../footer.jspf" %> 
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>       
</body>
</html>