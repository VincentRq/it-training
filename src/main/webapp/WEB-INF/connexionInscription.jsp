<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>IT-Training</title>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>   
<link rel ="stylesheet" href="css/ConnexionInscription.css">
         
</head>
<body>

<%@ include file="header.jspf" %>

<div id="container">
<main>
	<div class="body" id="firstBody">
		<div class="form">
		    <form class="formulaire" method="POST" action="connexionInscription">
		        <h1>Connexion</h1>
		        <label>E-mail</label>
		        <input id="email" class="input" type="email" name="email" required maxlength="200"/>
		        <br/> <br>
		        <label>Mot de passe</label>
		        <input  id="password" class="input" type="password" name="mdp" required maxlength="200"/>
		        <br> <br>
		        <p><a href="#">Vous avez oublié votre mot de passe ?          </a></p>
		        <br/>
		        <input class="bouton" type="submit" value="Connexion" name="connexion"/>
		        <br><br>
		        <p class="erreur">${messageErreurConnexion}</p>
		    </form>
		    <form class="formulaire" method="POST" action="connexionInscription">
		        <h1>Inscription</h1>
		        <label>E-mail</label>
		        <input  id="email" class="input" type="email" name="email" placeholder=".com ou .fr" required required maxlength="200"/>
		        <p class="erreur">${erreurs.get('email')}</p>
		        <br/>
		        <label>Entreprise</label>
		        <input id="entreprise" class="input" type="text" name="entreprise" placeholder="Facultatif"/>
		        <br> <br>
		        <label>Mot de passe</label>
		        <input id="mdp" class="input" type="password"name="mdp" placeholder="8 caractères minimum" required maxlength="200" />
		        <p class="erreur">${erreurs.get('mdp')}</p>
		        <br/>
		        <label>Confirmation du mot de passe</label>
		        <input id="verfiMdp" class="input" type="password" name="verifMdp" placeholder="8 caractères minimum" required maxlength="200"/>
		        <p class="erreur">${erreurs.get('verifMdp')}</p>
		        <br/>
		        <input class="bouton" type="submit" value="Inscription" name="inscription"/>
		        <br><br>
		        <p class="erreur">${messageSuccess}</p>
		     </form>
		</div>	
	</div>
</main>
</div>

	
<%@ include file="footer.jspf" %> 
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>          
</body>
</html>