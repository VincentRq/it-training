<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Tableau de bord</title>
<link rel="stylesheet" href="css/Base.css">
<link rel="stylesheet" href="css/PageListe.css">
</head>
<%@ include file="header.jspf" %>
<body>
<div id="container">
<main>
	<div class="body" id="firstBody">
		<h1>Bienvenue sur votre tableau de bord</h1>
		<br/>
		<div>
			<a class="bouton" href="deconnexion">Se déconnecter</a>
		</div>
		<section id="salleListe">
			<h1>Liste de mes formations</h1>
			<br/>
			<table>
				<thead>
					<tr>
						<th>Titre</th>
						<th>Date de début</th>
						<th>Date de fin</th>
						<th>Salle</th>
						<th>Adresse</th>
						<th>Code postal</th>
						<th>Ville</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${sessions}" var="session">
						<tr>
							<td>${session.catalogue.titre}</td>
							<td>${session.dateDebut}</td>
							<td>${session.dateFin}</td>
							<td>${session.salle.nom}</td>
							<td>${session.salle.adresse}</td>
							<td>${session.salle.codePostal}</td>
							<td>${session.salle.ville}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</section>
	</div>
</main>
</div>
<%@ include file="footer.jspf" %> 
</body>
</html>