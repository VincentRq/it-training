<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Contact</title>
<link rel ="stylesheet" href="css/Base.css">
<link rel ="stylesheet" href="css/Contact.css">
          
</head>
<body>
<%@ include file="header.jspf" %>

<div id="container">
<main>
	<div class="body" id="firstBody">
		<h1>Contact</h1><br/>
		<p>Pour des formations personnalisée, contactez : <a href="mailto:responsable-formation@it-training.com">responsable-formation@it-training.com</a></p><br/>
		<p>Pour parler à un de nos conseiller, contactez : <a href="mailto:commercial@it-training.com">commercial@it-training.com</a></p><br/>
		<p>Pour toute autre demande, contactez : <a href="mailto:admin@it-training.com">admin@it-training.com</a></p><br/>
		<div id="divMap">
			<div id="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1331.4296757243753!2d-1.640550527949774!3d48.13223569632714!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x480ede93effce075%3A0x504cbdbf797e88ae!2sib%20-%20groupe%20Cegos%20Rennes!5e0!3m2!1sfr!2sfr!4v1646641125558!5m2!1sfr!2sfr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe></div>
		</div>
	</div>
</main>
</div>
<%@ include file="footer.jspf" %>         
</body>
</html>