/* Remplissage les tables */

INSERT INTO Salle VALUES('Chez le Client', '', 'Chez le Client', 'Chez le Client', 'Chez le Client', 'Chez le Client');
INSERT INTO Salle VALUES('Mercure', '15', '12 rue du Patis Tatelin', '35700', 'RENNES', 'FRANCE');
INSERT INTO Salle VALUES('V�nus', '15', '12 rue du Patis Tatelin', '35700', 'RENNES', 'FRANCE');
INSERT INTO Salle VALUES('Mars', '15', '12 rue du Patis Tatelin', '35700', 'RENNES', 'FRANCE');
INSERT INTO Salle VALUES('Jupiter', '15', '12 rue du Patis Tatelin', '35700', 'RENNES', 'FRANCE');
INSERT INTO Salle VALUES('Saturne', '15', '12 rue du Patis Tatelin', '35700', 'RENNES', 'FRANCE');
INSERT INTO Salle VALUES('Uranus', '15', '12 rue du Patis Tatelin', '35700', 'RENNES', 'FRANCE');
INSERT INTO Salle VALUES('Neptune', '15', '12 rue du Patis Tatelin', '35700', 'RENNES', 'FRANCE');

INSERT INTO Utilisateur VALUES('admin@gmail.com','25D55AD283AA400AF464C76D713C07AD',null,1);
INSERT INTO Utilisateur VALUES('jean-michel@gmail.com','25D55AD283AA400AF464C76D713C07AD',null,0);
INSERT INTO Utilisateur VALUES('martinmartin@gmail.com','25D55AD283AA400AF464C76D713C07AD',null,0);
INSERT INTO Utilisateur VALUES('micheline418@gmail.com','25D55AD283AA400AF464C76D713C07AD',null,0);
INSERT INTO Utilisateur VALUES('charlotte.bourgeois@gmail.com','25D55AD283AA400AF464C76D713C07AD',null,0);
SELECT * FROM UTILISATEUR;

INSERT INTO Formateur VALUES('Delpierre', 'Adrien', '01/01/2016', '5' , 'adrien.delpierre@gmail.com', '42 rue Java', '35000', 'RENNES', 'FRANCE', '0');
INSERT INTO Formateur VALUES('Postec', 'Guillaume', '12/05/2021', '1.8' , 'guillaume.postec@gmail.com', '42 rue Java', '35000', 'RENNES', 'FRANCE', '0');
INSERT INTO Formateur VALUES('Roquet', 'Vincent', '05/04/2019', '4.3' , 'vincent.roquet@gmail.com', '42 rue Java', '35000', 'RENNES', 'FRANCE', '0');
INSERT INTO Formateur VALUES('Le Blond', 'Pascal', '30/09/2019', '4.6' , 'leblond.pascal10290@gmail.com', '42 rue Java', '35000', 'RENNES', 'FRANCE', '0');

INSERT INTO CATALOGUE VALUES ('Python','Informatique','Developpement informatique','Python','21','4','16','Issu du monde Open Source, Python est un langage r�put� facile d acc�s, simple et particuli�rement portable. Il propose aux d�veloppeurs un ensemble d outils et de fonctionnalit�s facilitant leurs t�ches (piles de modules, pas de n�cessit� de compilation, d�buggeur int�gr�, Shell de tests, documentation riche...). Mais avant de pouvoir b�n�ficier de tous ces avantages, les nouveaux d�veloppeurs Python devront s�approprier les notions essentielles de programmation objet et apprendre la syntaxe du langage. En suivant cette formation de 5 jours, les participants couvriront tous les sujets leur permettant de concevoir, d�velopper, d�ployer et maintenir des applications Python.','Disposer de connaissances de base en programmation (id�alement en langage objet)','Les identifiants et les r�f�rences
Les conventions de codage et les r�gles de nommage
Les blocs, les commentaires
Les types de donn�es disponibles
Les variables, l affichage format�, la port�e locale et globale
La manipulation des types num�riques, la manipulation de cha�nes de caract�res
La manipulation des tableaux dynamiques (liste), des tableaux statiques (tuple) et des dictionnaires
L utilisation des fichiers
La structure conditionnelle if/elif/else
Les op�rateurs logiques et les op�rateurs de comparaison
Les boucles d�it�rations while et for
Interruption d�it�rations break/continue
La fonction range
L �criture et la documentation de fonctions
Les Lambda expression
Les g�n�rateursLa structuration du code en modules',1400.20);
INSERT INTO CATALOGUE VALUES ('Java','Informatique','Developpement informatique','Java','21','4','16','Le langage Java est au coeur des applications d�entreprise et a r�ussi � s�imposer comme l�un des langages orient�-objet les plus utilis� dans l�industrie. Cette formation a pour objectif d�introduire les diff�rents concepts orient�s-objets en Java et de permettre une initiation progressive aux diff�rentes librairies et frameworks du langage. Les entr�es-sorties, les collections, l�acc�s aux donn�es, les exceptions, les nouveaut�s Java 9 ainsi qu�un certain nombre de librairies utilitaires seront pr�sent�es.','Disposer d�une exp�rience d�un langage de programmation (C, C++, VB...)
Conna�tre les principes de la programmation orient�e objet
Connaissance des concepts de bases de donn�es relationnelles et du langage SQL
Avoir d�j� d�velopp� et livr� une application est un plus pour suivre cette formation','D�claration de variables
Les op�rateurs
Initialisation
Instructions de contr�le
Boucles et it�rations
Notions de visibilit� et de variable de classe Vs. variable d�instance',1300.50);
INSERT INTO CATALOGUE VALUES ('PHP','Informatique','Developpement informatique','PHP','21','4','16','PHP, langage de programmation multi plates-formes, s est impos� comme un standard du march�. Cette technologie est pl�biscit�e pour sa capacit� � faciliter la cr�ation de sites dynamiques et marchands. PHP permet �galement de mieux adapter les pages � la diversit� des navigateurs et de leurs versions. Il simplifie enfin l acc�s aux bases de donn�es, notamment la base libre commun�ment utilis�e dans le monde de l Open Source : MySQL. En 4 jours, les participants � cette formation apprendront � d�velopper avec ce langage.','Disposer de connaissances pratiques de HTML et au moins d�un langage de programmation','Utilisation de PHP dans des fichiers HTML
Les variables et les op�rateurs
Les structures de contr�les (if, while, ...)
Les fonctions PHP
Les fonctions utilisateur
La gestion des fichiers', 1600);
INSERT INTO CATALOGUE VALUES ('Developpeur Java debutant','Informatique','Developpement informatique','Java','21','4','16','Description Blabla blabla','Prerequis Blabla blabla','Programme Blabla blabla',1800);
INSERT INTO CATALOGUE VALUES ('Developpeur PHP debutant','Informatique','Developpement informatique','PHP','14','3','16','Description Blabla blablo','Prerequis Blabla blablain','Programme Blabla blabla',1000);
INSERT INTO CATALOGUE VALUES ('Management : les fondamentaux','Management','Formations Management hierarchique et non hierarchique','','28','3','20','Description Blabla ','Prerequis Blabla ','Programme blabla',1500);
INSERT INTO CATALOGUE VALUES ('Formations communication, gestion des conflits','Management','Formations Communication, gestion des conflits','','7','4','16','Description Blabla blabla','Prerequis Blabla blabla','Programme Blabla blabla',1100);
INSERT INTO CATALOGUE VALUES ('Management a distance','Management','Formations Management hierarchique et non hierarchique','','14','3','18','Description Blabla blabla','Prerequis Blabla blabla','Programme Blabla blabla',1600);
INSERT INTO CATALOGUE VALUES ('Excel avance','Bureautique','Excel','','7','6','20','Description Blabla blabla','Prerequis Blabla blabla','Programme Blabla blabla',1200);
INSERT INTO CATALOGUE VALUES ('Outlook version 2020','Bureautique','Outlook','','7','6','20','Description Blabla blabla','Prerequis Blabla blabla','Programme Blabla blabla',800);
INSERT INTO CATALOGUE VALUES ('Outlook version 2008','Bureautique','Outlook','','7','6','20','Description Blabla blabla','Prerequis Blabla blabla','Programme Blabla blabla',500);
INSERT INTO CATALOGUE VALUES ('Presenter avec Power Point','Bureautique','PowerPoint','','7','6','20','Description Blabla blabla','Prerequis Blabla blabla','Programme Blabla blabla',1000);
INSERT INTO CATALOGUE VALUES ('Presenter avec Power Point II','Bureautique','PowerPoint','','7','6','20','Description Blabla blabla','Prerequis Blabla blabla','Programme Blabla blabla',600);
INSERT INTO CATALOGUE VALUES ('Animer sur Power Point','Bureautique','PowerPoint','','7','6','20','Description Blabla blabla','Prerequis Blabla blabla','Programme Blabla blabla',300);
INSERT INTO CATALOGUE VALUES ('PHP','Informatique','Developpement informatique','PHP','21','4','16','PHP, langage de programmation multi plates-formes, s est impos� comme un standard du march�. Cette technologie est pl�biscit�e pour sa capacit� � faciliter la cr�ation de sites dynamiques et marchands. PHP permet �galement de mieux adapter les pages � la diversit� des navigateurs et de leurs versions. Il simplifie enfin l acc�s aux bases de donn�es, notamment la base libre commun�ment utilis�e dans le monde de l Open Source : MySQL. En 4 jours, les participants � cette formation apprendront � d�velopper avec ce langage.','Disposer de connaissances pratiques de HTML et au moins d�un langage de programmation','Utilisation de PHP dans des fichiers HTML',1250.50);

INSERT INTO Session VALUES('2','1','1','2','26/05/2022','30/05/2022','0','inter');
INSERT INTO Session VALUES('2','1','1','3','12/03/2022','27/03/2022','0','inter');
INSERT INTO Session VALUES('2','1','1','8','01/04/2022','16/04/2022','0','inter');
INSERT INTO Session VALUES('2','1','1','6','24/06/2022','09/07/2022','0','inter');
INSERT INTO Session VALUES('1','2','1','3','30/06/2022','16/07/2022','0','inter');
INSERT INTO Session VALUES('1','2','1','1','18/03/2022','3/04/2022','0','intra');
INSERT INTO Session VALUES('1','2','1','2','12/05/2022','27/05/2022','0','inter');
INSERT INTO Session VALUES('1','2','1','7','10/04/2022','25/04/2022','0','inter');
INSERT INTO Session VALUES('3','3','1','3','30/06/2022','16/07/2022','0','inter');
INSERT INTO Session VALUES('3','3','1','5','18/03/2022','3/04/2022','0','inter');
INSERT INTO Session VALUES('3','3','1','2','12/05/2022','27/05/2022','0','inter');
INSERT INTO Session VALUES('3','3','1','4','10/04/2022','25/04/2022','0','inter');
INSERT INTO Session VALUES('9','4','1','3','30/06/2022','16/07/2022','0','inter');
INSERT INTO Session VALUES('9','4','1','5','18/03/2022','3/04/2022','0','inter');
INSERT INTO Session VALUES('9','4','1','2','12/05/2022','27/05/2022','0','inter');
INSERT INTO Session VALUES('9','4','1','3','10/04/2022','25/04/2022','0','inter');
Select * FROM Session;

INSERT INTO STAGIAIRE VALUES('2','Michel','Jean','35 rue de Rennes','35700','Rennes','France');
INSERT INTO STAGIAIRE VALUES('3','Martin','Martin','64 rue de Belgique','35000','Rennes','France');
INSERT INTO STAGIAIRE VALUES('4','Smith','Micheline','50 rue du Plesis','35700','Rennes','France');
INSERT INTO STAGIAIRE VALUES('5','Belatar','Mohammed','203 rue de Saint-Malo','35000','Rennes','France');
SELECT * FROM STAGIAIRE;


