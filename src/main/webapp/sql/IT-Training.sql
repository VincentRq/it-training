DROP DATABASE IT_training;
CREATE DATABASE IT_training;
USE IT_training;

CREATE TABLE CATALOGUE(
	id int IDENTITY PRIMARY KEY,
	titre varchar(200) not null,
	domaine varchar(200) not null,
	theme varchar(200) not null,
	sousTheme varchar(200),
	dureeH int,
	nbStagiaireMin int,
	nbStagiaireMax int,
	description varchar(4000),
	prerequis varchar(4000),
	programme varchar(4000),
	prix decimal(7,2)
);

CREATE TABLE FORMATEUR(
	id int IDENTITY PRIMARY KEY,
	nom varchar(200),
	prenom varchar(200),
	dateRecrutement date,
	note decimal(2,1),
	email varchar(200),
	adresse varchar(200),
	codePostal varchar(20),
	ville varchar(200),
	pays varchar(200),
	blame int
);

CREATE TABLE UTILISATEUR(
	id int IDENTITY PRIMARY KEY,
	email varchar(200),
	motDePasse varchar(200),
	entreprise varchar(200),
	isAdmin bit
);

CREATE TABLE SALLE(
	id int IDENTITY PRIMARY KEY,
	nom varchar(200),
	capacite int,
	adresse varchar(200),
	codePostal varchar(20),
	ville varchar(200),
	pays varchar(200)
);

CREATE TABLE SESSION(
	id int IDENTITY PRIMARY KEY,
	codeCatalogue int CONSTRAINT FK_CATALOGUE_SESSION FOREIGN KEY REFERENCES CATALOGUE(id),
	codeFormateur int CONSTRAINT FK_FORMATEUR_SESSION FOREIGN KEY REFERENCES FORMATEUR(id),
	responsable int CONSTRAINT FK_UTILISATEUR_SESSION FOREIGN KEY REFERENCES UTILISATEUR(id),
	codeSalle int CONSTRAINT FK_SALLE_SESSION FOREIGN KEY REFERENCES SALLE(id),
	dateDebut date,
	dateFin date,
	nombreStagiaire int,
	typeFormation char(5),
);
CREATE TABLE STAGIAIRE (
	id int IDENTITY PRIMARY KEY,
	codeUtilisateur int CONSTRAINT FK_UTILISATEUR_STAGIAIRE FOREIGN KEY REFERENCES UTILISATEUR(id),
	nom varchar(200),
	prenom varchar(200),
	adressePostale varchar(200),
	codePostal varchar(20),
	ville varchar(200),
	pays varchar(200),
);

CREATE TABLE Session_Stagiaire(
    id int IDENTITY PRIMARY KEY,
	session int CONSTRAINT FK_Code_Session FOREIGN KEY(session)
    REFERENCES Session(id),
	stagiaire int CONSTRAINT FK_Code_stagiaire FOREIGN KEY(stagiaire)
    REFERENCES Stagiaire(id)
    );


