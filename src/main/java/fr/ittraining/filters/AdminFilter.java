package fr.ittraining.filters;

import java.io.IOException;

import fr.ittraining.modeles.objets.Utilisateur;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebFilter(urlPatterns = {"/tableauDeBordAdmin"})
public class AdminFilter implements Filter {

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {

		HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
		HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
		
		if(httpServletRequest.getSession().getAttribute("utilisateur") != null) {
			Utilisateur utilisateur = (Utilisateur) httpServletRequest.getSession().getAttribute("utilisateur");
			if (utilisateur.isAdmin()) {
				filterChain.doFilter(servletRequest, servletResponse);
			} else {
				httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/accueil?codeErreur=3");
			}
		} else {
			httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/accueil?codeErreur=1");
		}
	}
}
