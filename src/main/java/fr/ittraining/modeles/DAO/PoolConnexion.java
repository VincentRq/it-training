package fr.ittraining.modeles.DAO;

import java.sql.Connection;
import java.sql.DriverManager;

public class PoolConnexion {
	
	public static Connection getConnexion() {
		Connection connexion = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver"); // charger le driver
			connexion =	DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databasename=It_Training", "sa", "Password1234");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return connexion;
	}
}
