package fr.ittraining.modeles.DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.ittraining.modeles.objets.Catalogue;
import fr.ittraining.modeles.objets.Formateur;
import fr.ittraining.modeles.objets.Salle;
import fr.ittraining.modeles.objets.Session;
import fr.ittraining.modeles.objets.Utilisateur;

public class SessionDAO {

	public void enregistrer(Session session) {
		
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "INSERT INTO "
					+ "Session(codeCatalogue, codeFormateur, responsable, codeSalle, dateDebut, dateFin, nombreStagiaire, typeFormation) "
					+ "VALUES(?,?,?,?,?,?,?,?)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, session.getCatalogue().getId());
			pstmt.setInt(2, session.getFormateur().getId());
			pstmt.setInt(3, session.getResponsable().getId());
			pstmt.setInt(4, session.getSalle().getId());
			pstmt.setDate(5, Date.valueOf(session.getDateDebut()));
			pstmt.setDate(6, Date.valueOf(session.getDateFin()));
			pstmt.setInt(7, session.getNbStagiaire());
			pstmt.setString(8, session.getTypeFormation());
			pstmt.executeUpdate();
			connexion.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void modifier(Session session, int id) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "UPDATE Session "
					+ "SET codeCatalogue = ?, codeFormateur = ?, responsable = ?, codeSalle = ?, dateDebut = ?, dateFin = ?, nombreStagiaire = ?, typeFormation = ? "
					+ "WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, session.getCatalogue().getId());
			pstmt.setInt(2, session.getFormateur().getId());
			pstmt.setInt(3, session.getResponsable().getId());
			pstmt.setInt(4, session.getSalle().getId());
			pstmt.setDate(5, Date.valueOf(session.getDateDebut()));
			pstmt.setDate(6, Date.valueOf(session.getDateFin()));
			pstmt.setInt(7, session.getNbStagiaire());
			pstmt.setString(8, session.getTypeFormation());
			pstmt.setInt(9, id);
			pstmt.executeUpdate();
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Session> findAll() {
		List<Session> sessions = new ArrayList<>();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT *, S.id as Session_id, C.id as Catalogue_id, F.id as Formateur_id, U.id as Utilisateur_id, " 
					+ "Sa.id as Salle_id, Sa.nom as Salle_nom, F.nom as Formateur_nom, F.adresse as Formateur_adresse, "
					+ "F.codePostal as Formateur_codePostal, F.ville as Formateur_ville, F.pays as Formateur_pays,"
					+ "Sa.adresse as Salle_adresse, Sa.codePostal as Salle_codePostal, Sa.ville as Salle_ville, "
					+ "Sa.pays as Salle_pays "
					+ "FROM Session S "
					+ "JOIN Catalogue C ON (S.codeCatalogue = C.id) "
					+ "JOIN Formateur F ON (S.codeFormateur = F.id) "
					+ "JOIN Utilisateur U ON (S.responsable = U.id) "
					+ "JOIN Salle Sa ON (S.codeSalle = Sa.id) "
					+ "ORDER BY dateDebut ASC, dateFin ASC";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Session session = creerSession(rs);
				sessions.add(session);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sessions;
	}
	
	public List<Session> findAllByStagiaire(int stagiaireId) {
		List<Session> sessions = new ArrayList<>();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT *, S.id as Session_id, C.id as Catalogue_id, F.id as Formateur_id, U.id as Utilisateur_id, " 
					+ "Sa.id as Salle_id, Sa.nom as Salle_nom, F.nom as Formateur_nom, F.adresse as Formateur_adresse, "
					+ "F.codePostal as Formateur_codePostal, F.ville as Formateur_ville, F.pays as Formateur_pays,"
					+ "Sa.adresse as Salle_adresse, Sa.codePostal as Salle_codePostal, Sa.ville as Salle_ville, "
					+ "Sa.pays as Salle_pays "
					+ "FROM Session S "
					+ "JOIN Catalogue C ON (S.codeCatalogue = C.id) "
					+ "JOIN Formateur F ON (S.codeFormateur = F.id) "
					+ "JOIN Utilisateur U ON (S.responsable = U.id) "
					+ "JOIN Salle Sa ON (S.codeSalle = Sa.id)"
					+ "JOIN Session_Stagiaire SS ON (S.id = SS.session)"
					+ "WHERE SS.stagiaire = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, stagiaireId);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Session session = creerSession(rs);
				sessions.add(session);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sessions;
	}

	public Session findById(int sessionId) {
		Session session = new Session();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT *, S.id as Session_id, C.id as Catalogue_id, F.id as Formateur_id, U.id as Utilisateur_id, "
					+ "Sa.id as Salle_id, Sa.nom as Salle_nom, F.nom as Formateur_nom, F.adresse as Formateur_adresse, "
					+ "F.codePostal as Formateur_codePostal, F.ville as Formateur_ville, F.pays as Formateur_pays, "
					+ "Sa.adresse as Salle_adresse, Sa.codePostal as Salle_codePostal, Sa.ville as Salle_ville, "
					+ "Sa.pays as Salle_pays "
					+ "FROM Session S " 
					+ "JOIN Catalogue C ON (codeCatalogue = C.id) "
					+ "JOIN Formateur F ON (codeFormateur = F.id) "
					+ "JOIN Utilisateur U ON (responsable = U.id) "
					+ "JOIN Salle Sa ON (codeSalle =Sa.id) WHERE S.id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, sessionId);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				session = creerSession(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return session;
	}
	
	public void supprimer(int id) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "DELETE FROM Session WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			pstmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private Session creerSession(ResultSet rs) throws SQLException {
		Catalogue catalogue = new Catalogue();
		Formateur formateur = new Formateur();
		Utilisateur responsable = new Utilisateur();
		Salle salle = new Salle();
		Session session = new Session();
		
		catalogue.setId(rs.getInt("Catalogue_id"));
		catalogue.setTitre(rs.getString("titre"));
		catalogue.setDomaine(rs.getString("domaine"));
		catalogue.setTheme(rs.getString("theme"));
		catalogue.setSousTheme(rs.getString("sousTheme"));
		catalogue.setDureeH(rs.getInt("dureeH"));
		catalogue.setNbStagiaireMin(rs.getInt("nbStagiaireMin"));
		catalogue.setNbStagiaireMax(rs.getInt("nbStagiaireMax"));
		catalogue.setPrerequis(rs.getString("prerequis"));
		catalogue.setDescription(rs.getString("description"));
		catalogue.setProgramme(rs.getString("programme"));
		
		formateur.setId(rs.getInt("Formateur_id"));
		formateur.setNom(rs.getString("Formateur_nom"));
		formateur.setPrenom(rs.getString("prenom"));
		formateur.setDateRecrutement(LocalDate.parse(rs.getString("dateRecrutement")));
		formateur.setNote(rs.getDouble("note"));
		formateur.setEmail(rs.getString("email"));
		formateur.setAdresse(rs.getString("Formateur_adresse"));
		formateur.setCodePostal(rs.getString("Formateur_codePostal"));
		formateur.setVille(rs.getString("Formateur_ville"));
		formateur.setPays(rs.getString("Formateur_pays"));
		formateur.setBlame(rs.getInt("blame"));
		
		responsable.setId(rs.getInt("Utilisateur_id"));
		responsable.setEmail(rs.getString("email"));
		responsable.setMdp(rs.getString("motDePasse"));
		responsable.setAdmin(rs.getBoolean("isAdmin"));
		
		salle.setId(rs.getInt("Salle_id"));
		salle.setNom(rs.getString("Salle_nom"));
		salle.setCapacite(rs.getInt("capacite"));
		salle.setAdresse(rs.getString("Salle_adresse"));
		salle.setCodePostal(rs.getString("Salle_codePostal"));
		salle.setVille(rs.getString("Salle_ville"));
		salle.setPays(rs.getString("Salle_pays"));

		session.setId(rs.getInt("Session_id"));
		session.setCatalogue(catalogue);
		session.setFormateur(formateur);
		session.setResponsable(responsable);
		session.setSalle(salle);
		session.setDateDebut(LocalDate.parse(rs.getString("dateDebut")));
		session.setDateFin(LocalDate.parse(rs.getString("dateFin")));
		session.setNbStagiaire(rs.getInt("nombreStagiaire"));
		session.setTypeFormation(rs.getString("typeFormation"));
		return session;
	}

	public List<Session> findSessionbyIdCatalogue(int id) {
		List<Session> sessions = new ArrayList<>();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT *, S.id as Session_id, C.id as Catalogue_id, F.id as Formateur_id, U.id as Utilisateur_id, " 
					+ "Sa.id as Salle_id, Sa.nom as Salle_nom, F.nom as Formateur_nom, F.adresse as Formateur_adresse, "
					+ "F.codePostal as Formateur_codePostal, F.ville as Formateur_ville, F.pays as Formateur_pays,"
					+ "Sa.adresse as Salle_adresse, Sa.codePostal as Salle_codePostal, Sa.ville as Salle_ville, "
					+ "Sa.pays as Salle_pays "
					+ "FROM Session S "
					+ "JOIN Catalogue C ON (S.codeCatalogue = C.id) "
					+ "JOIN Formateur F ON (S.codeFormateur = F.id) "
					+ "JOIN Utilisateur U ON (S.responsable = U.id) "
					+ "JOIN Salle Sa ON (S.codeSalle = Sa.id)"
					+ "WHERE codeCatalogue = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Session session = creerSession(rs);
				sessions.add(session);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sessions;
	}

	public List<Session> findSessionbyIdCatalogueAndNotAlreadySubscribed(int idCatalogue, int idUtilisateur) {
		List<Session> sessions = new ArrayList<>();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT id, dateDebut, codeCatalogue FROM Session "
					+ "WHERE codeCatalogue = ? "
					+ "except "
					+ "SELECT S.id, S.dateDebut, S.codeCatalogue FROM Session S "
					+ "JOIN Session_Stagiaire SS ON (S.id = SS.session) "
					+ "JOIN Stagiaire ST ON (SS.stagiaire = ST.id) "
					+ "WHERE ST.codeUtilisateur = ? ";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, idCatalogue);
			pstmt.setInt(2, idUtilisateur);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Session session = new Session();
				session.setId(rs.getInt("id"));
				session.setDateDebut(LocalDate.parse(rs.getString("dateDebut")));
				sessions.add(session);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return sessions;
	}

}
