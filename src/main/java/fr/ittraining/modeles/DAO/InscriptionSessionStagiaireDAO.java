package fr.ittraining.modeles.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.ittraining.modeles.objets.Stagiaire;
import fr.ittraining.modeles.objets.Utilisateur;

public class InscriptionSessionStagiaireDAO {
	
	Utilisateur utilisateur = new Utilisateur();
	
	public void enregistrerStagiaire(Stagiaire stagiaire) {

	try {
		Connection connexion = PoolConnexion.getConnexion();
		String query = "INSERT INTO Stagiaire(codeUtilisateur, nom, prenom, adressePostale, codePostal, ville, pays) "
				+ " VALUES(?, ?, ?, ?, ?, ?, ?);";
		PreparedStatement pstmt = connexion.prepareStatement(query);
		pstmt.setInt(1, stagiaire.getCodeUtilisateur().getId());
		pstmt.setString(2, stagiaire.getNom());
		pstmt.setString(3, stagiaire.getPrenom());
		pstmt.setString(4, stagiaire.getAdressePostale());
		pstmt.setString(5, stagiaire.getCodePostal());
		pstmt.setString(6, stagiaire.getVille());
		pstmt.setString(7, stagiaire.getPays());
		pstmt.executeUpdate();
		connexion.close();
	} catch (Exception e){
		e.printStackTrace();
	}
}

	public void enregistrerSessionStagiaire(int idStagiaire, int sessionId) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "INSERT INTO session_stagiaire(session, stagiaire) values(?, ?)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, sessionId);
			pstmt.setInt(2, idStagiaire);
			pstmt.executeUpdate();
			connexion.close();
		} catch (Exception e){
			e.printStackTrace();
		}
			
	}
	
	public boolean sessionStagiaireExist(int idSession, int idStagiaire) {
		List<Integer> listeSession = new ArrayList<>();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Session_Stagiaire where stagiaire = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, idStagiaire);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				listeSession.add(rs.getInt("session"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(listeSession.contains(idSession)) {
			return true;
		} else {
			return false;
		}
	}

}
