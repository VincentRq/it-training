package fr.ittraining.modeles.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.ittraining.modeles.objets.Salle;

public class SalleDAO {

	public void enregistrer(Salle salle) {
		
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "INSERT INTO "
					+ "Salle(nom, capacite, adresse, codePostal, ville, pays) "
					+ "VALUES(?,?,?,?,?,?)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, salle.getNom());
			pstmt.setInt(2, salle.getCapacite());
			pstmt.setString(3, salle.getAdresse());
			pstmt.setString(4, salle.getCodePostal());
			pstmt.setString(5, salle.getVille());
			pstmt.setString(6, salle.getPays());
			pstmt.executeUpdate();
			connexion.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void modifier(Salle salle, int id) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "UPDATE Salle "
					+ "SET nom = ?, capacite = ?, adresse = ?, codePostal = ?, ville = ?, pays = ? "
					+ "WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, salle.getNom());
			pstmt.setInt(2, salle.getCapacite());
			pstmt.setString(3, salle.getAdresse());
			pstmt.setString(4, salle.getCodePostal());
			pstmt.setString(5, salle.getVille());
			pstmt.setString(6, salle.getPays());
			pstmt.setInt(7, id);
			pstmt.executeUpdate();
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Salle> findAll() {
		List<Salle> salles = new ArrayList<>();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Salle";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Salle salle = creerSalle(rs);
				salles.add(salle);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return salles;
	}

	public Salle findById(int id) {
		Salle salle = new Salle();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Salle WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				salle = creerSalle(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return salle;
	}
	
	private Salle creerSalle(ResultSet rs) throws SQLException {
		Salle salle = new Salle();
		salle.setId(rs.getInt("id"));
		salle.setNom(rs.getString("nom"));
		salle.setCapacite(rs.getInt("capacite"));
		salle.setAdresse(rs.getString("adresse"));
		salle.setCodePostal(rs.getString("codePostal"));
		salle.setVille(rs.getString("ville"));
		salle.setPays(rs.getString("pays"));
		return salle;
	}

	public void supprimer(int id) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "DELETE FROM Salle WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
