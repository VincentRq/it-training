package fr.ittraining.modeles.DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.ittraining.modeles.objets.Stagiaire;
import fr.ittraining.modeles.objets.Stagiaire;
import fr.ittraining.modeles.objets.Utilisateur;

public class StagiaireDAO {

	Utilisateur utilisateur = new Utilisateur();
	
	public int findStagiaireIdByCodeUtilisateur(int codeUtilisateur) {
		int idStagiaire = 0;
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Stagiaire where codeUtilisateur = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, codeUtilisateur);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				idStagiaire = rs.getInt("id");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return idStagiaire;
	}
	
	public Stagiaire findStagiaireByCodeUtilisateur(int codeUtilisateur) {
		Stagiaire stagiaire = new Stagiaire();
		try {
			System.out.println("checkDAO");
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Stagiaire where codeUtilisateur = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, codeUtilisateur);
			ResultSet rs = pstmt.executeQuery();
			System.out.println(rs);
			while(rs.next()) {
				stagiaire = creerStagiaire(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return stagiaire;
	}
	
	public List<Integer> findAllCodesUtilisateurs() {
		List<Integer> listeCodesUtilisateurs = new ArrayList<>();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT codeUtilisateur FROM Stagiaire";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				listeCodesUtilisateurs.add(rs.getInt("codeUtilisateur"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listeCodesUtilisateurs;
	}

	public Stagiaire findById(int idStagiaire) {
		Stagiaire stagiaire = new Stagiaire();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Stagiaire WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, idStagiaire);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				stagiaire = creerStagiaire(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return stagiaire;
	}

	public void supprimer(int idStagiaire) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "DELETE FROM Stagiaire WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, idStagiaire);
			ResultSet rs = pstmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void enregistrer(Stagiaire stagiaire) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "INSERT INTO "
					+ "Stagiaire(codeUtilisateur, nom, prenom, adressePostale, codePostal, ville, pays) "
					+ "VALUES(?,?,?,?,?,?,?)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, stagiaire.getCodeUtilisateur().getId());
			pstmt.setString(2, stagiaire.getNom());
			pstmt.setString(3, stagiaire.getPrenom());
			pstmt.setString(4, stagiaire.getAdressePostale());
			pstmt.setString(5, stagiaire.getCodePostal());
			pstmt.setString(6, stagiaire.getVille());
			pstmt.setString(7, stagiaire.getPays());
			pstmt.executeUpdate();
			connexion.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void modifier(Stagiaire stagiaire, int id) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "UPDATE Stagiaire "
					+ "SET codeUtilisateur = ?, nom = ?, prenom = ?, "
					+ "adressePostale = ?, codePostal = ?, ville = ?, pays = ? "
					+ "WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, stagiaire.getCodeUtilisateur().getId());
			pstmt.setString(2, stagiaire.getNom());
			pstmt.setString(3, stagiaire.getPrenom());
			pstmt.setString(4, stagiaire.getAdressePostale());
			pstmt.setString(5, stagiaire.getCodePostal());
			pstmt.setString(6, stagiaire.getVille());
			pstmt.setString(7, stagiaire.getPays());
			pstmt.setInt(8, id);
			pstmt.executeUpdate();
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Stagiaire> findAll() {
		List<Stagiaire> stagiaires = new ArrayList<>();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Stagiaire";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Stagiaire stagiaire = creerStagiaire(rs);
				stagiaires.add(stagiaire);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return stagiaires;
	}
	
	
	public Stagiaire creerStagiaire(ResultSet rs) throws SQLException {
		Stagiaire stagiaire = new Stagiaire();
		stagiaire.setId(rs.getInt("id"));
		Utilisateur utilisateur = new UtilisateurDAO().findById(rs.getInt("codeUtilisateur"));
		stagiaire.setCodeUtilisateur(utilisateur);
		stagiaire.setNom(rs.getString("nom"));
		stagiaire.setPrenom(rs.getString("prenom"));
		stagiaire.setAdressePostale(rs.getString("adressePostale"));
		stagiaire.setCodePostal(rs.getString("codePostal"));
		stagiaire.setVille(rs.getString("ville"));
		stagiaire.setPays(rs.getString("pays"));
		return stagiaire;
	}

}
