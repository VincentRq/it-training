package fr.ittraining.modeles.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.ittraining.modeles.objets.Catalogue;
import fr.ittraining.modeles.objets.Catalogue;

public class CatalogueDAO {

	public void enregistrer(Catalogue catalogue) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "INSERT INTO "
					+ "Catalogue(titre, domaine, theme, sousTheme, dureeH, prix, nbStagiaireMin, nbStagiaireMax, prerequis, description, programme) "
					+ "VALUES(?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, catalogue.getTitre());
			pstmt.setString(2, catalogue.getDomaine());
			pstmt.setString(3, catalogue.getTheme());
			pstmt.setString(4, catalogue.getSousTheme());
			pstmt.setInt(5, catalogue.getDureeH());
			pstmt.setDouble(6, catalogue.getPrix());
			pstmt.setInt(7, catalogue.getNbStagiaireMin());
			pstmt.setInt(8, catalogue.getNbStagiaireMax());
			pstmt.setString(9, catalogue.getPrerequis());
			pstmt.setString(10, catalogue.getDescription());
			pstmt.setString(11, catalogue.getProgramme());
			pstmt.executeUpdate();
			connexion.close();
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	public List<Catalogue> findAll() {
		List<Catalogue> catalogues = new ArrayList<>();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Catalogue";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Catalogue catalogue = creerCatalogue(rs);
				catalogues.add(catalogue);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return catalogues;
	}

	private Catalogue creerCatalogue(ResultSet rs) throws SQLException {
		Catalogue catalogue = new Catalogue();
		catalogue.setId(rs.getInt("id"));
		catalogue.setTitre(rs.getString("titre"));
		catalogue.setDomaine(rs.getString("domaine"));
		catalogue.setTheme(rs.getString("theme"));
		catalogue.setSousTheme(rs.getString("sousTheme"));
		catalogue.setDureeH(rs.getInt("dureeH"));
		catalogue.setPrix(rs.getDouble("prix"));
		catalogue.setNbStagiaireMin(rs.getInt("nbStagiaireMin"));
		catalogue.setNbStagiaireMax(rs.getInt("nbStagiaireMax"));
		catalogue.setDescription(rs.getString("description"));
		catalogue.setProgramme(rs.getString("programme"));
		catalogue.setPrerequis(rs.getString("prerequis"));
		return catalogue;
	}

	public Catalogue findById(int id) {
		Catalogue catalogue = new Catalogue();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Catalogue WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				catalogue = creerCatalogue(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return catalogue;
	}

	public void modifier(Catalogue catalogue, int id) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "UPDATE Catalogue "
					+ "SET titre = ?, domaine = ?, theme = ?, sousTheme = ?, dureeH = ?, prix = ?, nbStagiaireMin = ?, nbStagiaireMax = ?, prerequis = ?, description = ?, programme = ? "
					+ "WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, catalogue.getTitre());
			pstmt.setString(2, catalogue.getDomaine());
			pstmt.setString(3, catalogue.getTheme());
			pstmt.setString(4, catalogue.getSousTheme());
			pstmt.setInt(5, catalogue.getDureeH());
			pstmt.setDouble(6, catalogue.getPrix());
			pstmt.setInt(7, catalogue.getNbStagiaireMin());
			pstmt.setInt(8, catalogue.getNbStagiaireMax());
			pstmt.setString(9, catalogue.getPrerequis());
			pstmt.setString(10, catalogue.getDescription());
			pstmt.setString(11, catalogue.getProgramme());
			pstmt.setInt(12, id);
			pstmt.executeUpdate();
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void supprimer(int id) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "DELETE FROM Catalogue WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Catalogue> findByDomaine(String domaine) {
		List<Catalogue> catalogues = new ArrayList<>();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Catalogue WHERE domaine = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, domaine);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Catalogue catalogue = creerCatalogue(rs);
				catalogues.add(catalogue);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return catalogues;
	}

}
