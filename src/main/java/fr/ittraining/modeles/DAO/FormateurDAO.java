package fr.ittraining.modeles.DAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.ittraining.modeles.objets.Formateur;

public class FormateurDAO {

	public void enregistrer(Formateur formateur) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "INSERT INTO "
					+ "Formateur(nom, prenom, dateRecrutement, note, email, adresse, codePostal, ville, pays, blame) "
					+ "VALUES(?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, formateur.getNom());
			pstmt.setString(2, formateur.getPrenom());
			pstmt.setDate(3, Date.valueOf(formateur.getDateRecrutement()));
			pstmt.setDouble(4, formateur.getNote());
			pstmt.setString(5, formateur.getEmail());
			pstmt.setString(6, formateur.getAdresse());
			pstmt.setString(7, formateur.getCodePostal());
			pstmt.setString(8, formateur.getVille());
			pstmt.setString(9, formateur.getPays());
			pstmt.setInt(10, formateur.getBlame());
			pstmt.executeUpdate();
			connexion.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void modifier(Formateur formateur, int id) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "UPDATE SALLE "
					+ "SET nom = ?, prenom = ?, dateRecrutement = ?, note = ?, email = ?, "
					+ "adresse = ?, codePostal = ?, ville = ?, pays = ?, blame = ? "
					+ "WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, formateur.getNom());
			pstmt.setString(2, formateur.getPrenom());
			pstmt.setDate(3, Date.valueOf(formateur.getDateRecrutement()));
			pstmt.setDouble(4, formateur.getNote());
			pstmt.setString(5, formateur.getEmail());
			pstmt.setString(6, formateur.getAdresse());
			pstmt.setString(7, formateur.getCodePostal());
			pstmt.setString(8, formateur.getVille());
			pstmt.setString(9, formateur.getPays());
			pstmt.setInt(10, formateur.getBlame());
			pstmt.setInt(11, id);
			pstmt.executeUpdate();
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Formateur> findAll() {
		List<Formateur> formateurs = new ArrayList<>();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Formateur";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Formateur formateur = creerFormateur(rs);
				formateurs.add(formateur);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return formateurs;
	}

	public Formateur findById(int formateurId) {
		Formateur formateur = new Formateur();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Formateur WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, formateurId);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				formateur = creerFormateur(rs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return formateur;
	}
	
	private Formateur creerFormateur(ResultSet rs) throws SQLException {
		Formateur formateur = new Formateur();
		formateur.setId(rs.getInt("id"));
		formateur.setNom(rs.getString("nom"));
		formateur.setPrenom(rs.getString("prenom"));
		formateur.setDateRecrutement(LocalDate.parse(rs.getString("dateRecrutement")));
		formateur.setNote(rs.getDouble("note"));
		formateur.setEmail(rs.getString("email"));
		formateur.setAdresse(rs.getString("adresse"));
		formateur.setCodePostal(rs.getString("codePostal"));
		formateur.setVille(rs.getString("ville"));
		formateur.setPays(rs.getString("pays"));
		formateur.setBlame(rs.getInt("blame"));
		return formateur;
	}

	public void supprimer(int id) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "DELETE FROM Formateur WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
