package fr.ittraining.modeles.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.ittraining.modeles.objets.Utilisateur;
import fr.ittraining.modeles.objets.Utilisateur;

public class UtilisateurDAO {

	public void enregistrerUtilisateur(Utilisateur utilisateur) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "INSERT INTO Utilisateur(email, motDePasse, entreprise, isAdmin) "
					+ " VALUES(?,CONVERT(NVARCHAR(32),HASHBYTES('MD5', cast(? as varchar)),2),?,0);";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, utilisateur.getEmail());
			pstmt.setString(2, utilisateur.getMdp());
			pstmt.setString(3, utilisateur.getEntreprise());
			pstmt.executeUpdate();
			connexion.close();
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public Utilisateur findByEmailAndMdp(String email, String mdp) {
		Utilisateur utilisateur = null;
		try {
		Connection connexion = PoolConnexion.getConnexion();
		String query = "SELECT * FROM Utilisateur WHERE email = ? and motDePasse = CONVERT(NVARCHAR(32), HASHBYTES('MD5', cast(? as varchar)),2)";
		PreparedStatement pstmt = connexion.prepareStatement(query);
		pstmt.setString(1, email);
		pstmt.setString(2, mdp);
		ResultSet rs = pstmt.executeQuery();
		while(rs.next()) {
			utilisateur = remplirUtilisateur(rs);
		}			
		
		} catch (Exception e){
			e.printStackTrace();
		} return utilisateur;
	}
	
	public static Utilisateur remplirUtilisateur(ResultSet rs) throws SQLException {
		Utilisateur utilisateur;
		utilisateur = new Utilisateur();
		utilisateur.setId(rs.getInt("id"));
		utilisateur.setEmail(rs.getString("email"));
		utilisateur.setMdp(rs.getString("motDePasse"));
		utilisateur.setEntreprise(rs.getString("entreprise"));
		utilisateur.setAdmin(rs.getBoolean("isAdmin"));
		return utilisateur;
	}


	public List<Utilisateur> findAllAdmin() {
		List<Utilisateur> utilisateurs = new ArrayList<>();
		try {
		Connection connexion = PoolConnexion.getConnexion();
		String query = "SELECT * FROM Utilisateur WHERE isAdmin = 1";
		PreparedStatement pstmt = connexion.prepareStatement(query);
		ResultSet rs = pstmt.executeQuery();
		while(rs.next()) {
			Utilisateur utilisateur = remplirUtilisateur(rs);
			utilisateurs.add(utilisateur);
		}			
		
		} catch (Exception e){
			e.printStackTrace();
		} return utilisateurs;
	}

	public Utilisateur findById(int id) {
		Utilisateur utilisateur = null;
		try {
		Connection connexion = PoolConnexion.getConnexion();
		String query = "SELECT * FROM Utilisateur WHERE id = ?";
		PreparedStatement pstmt = connexion.prepareStatement(query);
		pstmt.setInt(1, id);
		ResultSet rs = pstmt.executeQuery();
		while(rs.next()) {
			utilisateur = remplirUtilisateur(rs);
		}			
		
		} catch (Exception e){
			e.printStackTrace();
		} return utilisateur;
	}
	
	public List<Utilisateur> findAll() {
		List<Utilisateur> utilisateurs = new ArrayList<>();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT * FROM Utilisateur";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Utilisateur utilisateur = remplirUtilisateur(rs);
				utilisateurs.add(utilisateur);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return utilisateurs;

	}

	public void supprimer(int id) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "DELETE FROM Utilisateur WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void modifierUtilisateur(Utilisateur utilisateur, int id) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "UPDATE Utilisateur "
					+ "SET email = ?, motDePasse = ?, entreprise = ?, isAdmin = ? WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setString(1, utilisateur.getEmail());
			pstmt.setString(2, utilisateur.getMdp());
			pstmt.setString(3, utilisateur.getEntreprise());
			pstmt.setBoolean(4, utilisateur.getAdmin());
			pstmt.setInt(5, id);
			pstmt.executeUpdate();
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void nommerAdministrateur(int id) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "UPDATE Utilisateur "
					+ "SET isAdmin = 1 WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void destituerAdministrateur(int id) {
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "UPDATE Utilisateur "
					+ "SET isAdmin = 0 WHERE id = ?";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
			connexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<String> findAllEmail() {
		List<String> emails = new ArrayList<>();
		try {
			Connection connexion = PoolConnexion.getConnexion();
			String query = "SELECT email FROM Utilisateur";
			PreparedStatement pstmt = connexion.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				String email = rs.getString("email");
				emails.add(email);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return emails;

	}
}
