package fr.ittraining.modeles.objets;

public class Salle {
	
	private int id;
	private String nom;
	private int capacite;
	private String adresse;
	private String codePostal;
	private String ville;
	private String pays;
	
	
	public Salle() {
		super();
	}

	public Salle(String nom, int capacite, String adresse, String codePostal, String ville, String pays) {
		super();
		this.nom = nom;
		this.capacite = capacite;
		this.adresse = adresse;
		this.codePostal = codePostal;
		this.ville = ville;
		this.pays = pays;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getCapacite() {
		return capacite;
	}
	public void setCapacite(int capacite) {
		this.capacite = capacite;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}

	@Override
	public String toString() {
		return "Salle [id=" + id + ", nom=" + nom + ", capacite=" + capacite + ", adresse=" + adresse + ", codePostal="
				+ codePostal + ", ville=" + ville + ", pays=" + pays + "]";
	}
	
	
	
}
