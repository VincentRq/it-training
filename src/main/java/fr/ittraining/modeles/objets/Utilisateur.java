package fr.ittraining.modeles.objets;

public class Utilisateur {
	
	int id;
	String email;
	String entreprise;
	String mdp;
	Boolean admin;
	
	public Utilisateur() {
	}

	public Utilisateur(String email, String entreprise, String mdp) {
		super();
		this.email = email;
		this.entreprise = entreprise;
		this.mdp = mdp;
		this.admin = false;
	}

	public Utilisateur(String email, String entreprise, String mdp, boolean admin) {
		super();
		this.email = email;
		this.entreprise = entreprise;
		this.mdp = mdp;
		this.admin = admin;
	}

	public String getEmail() {
		return email;
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Boolean getAdmin() {
		return admin;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getEntreprise() {
		return entreprise;
	}

	public void setEntreprise(String entreprise) {
		this.entreprise = entreprise;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public Boolean isAdmin() {
		return admin;
	}

	public void setAdmin(Boolean isAdmin) {
		this.admin = isAdmin;
	}

	@Override
	public String toString() {
		return "Utilisateur [id=" + id + ", email=" + email + ", entreprise=" + entreprise + ", mdp=" + mdp
				+ ", admin=" + admin + "]";
	}
	
}
