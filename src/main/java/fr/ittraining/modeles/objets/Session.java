package fr.ittraining.modeles.objets;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class Session {

	private int id;
	private LocalDate dateDebut;
	private String dateDebutFR;
	private LocalDate dateFin;
	private String dateFinFR;
	private int nbStagiaire;
	private String typeFormation;
	private Catalogue catalogue;
	private Formateur formateur;
	private Utilisateur responsable;
	private Salle salle;
	
	public Session() {
	}

	public Session(LocalDate dateDebut, LocalDate dateFin, int nbStagiaire, String typeFormation, Catalogue catalogue,
			Formateur formateur, Utilisateur responsable, Salle salle) {
		super();
		this.dateDebut = dateDebut;
		this.setDateDebutFR();
		this.dateFin = dateFin;
		this.setDateFinFR();
		this.nbStagiaire = nbStagiaire;
		this.typeFormation = typeFormation;
		this.catalogue = catalogue;
		this.formateur = formateur;
		this.responsable = responsable;
		this.salle = salle;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
		this.setDateDebutFR();
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
		this.setDateFinFR();
	}

	public int getNbStagiaire() {
		return nbStagiaire;
	}

	public void setNbStagiaire(int nbStagiaire) {
		this.nbStagiaire = nbStagiaire;
	}

	public String getTypeFormation() {
		return typeFormation;
	}

	public void setTypeFormation(String typeFormation) {
		this.typeFormation = typeFormation;
	}

	public Catalogue getCatalogue() {
		return catalogue;
	}

	public void setCatalogue(Catalogue catalogue) {
		this.catalogue = catalogue;
	}

	public Formateur getFormateur() {
		return formateur;
	}

	public void setFormateur(Formateur formateur) {
		this.formateur = formateur;
	}

	public Utilisateur getResponsable() {
		return responsable;
	}

	public void setResponsable(Utilisateur responsable) {
		this.responsable = responsable;
	}

	public Salle getSalle() {
		return salle;
	}

	public void setSalle(Salle salle) {
		this.salle = salle;
	}

	public String getDateDebutFR() {
		return dateDebutFR;
	}

	private void setDateDebutFR() {
		this.dateDebutFR = this.dateDebut.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}

	public String getDateFinFR() {
		return dateFinFR;
	}

	private void setDateFinFR() {
		this.dateFinFR = this.dateFin.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}

	@Override
	public String toString() {
		return "Session [id=" + id + ", dateDebut=" + dateDebut + ", dateFin=" + dateFin + ", nbStagiaire="
				+ nbStagiaire + ", typeFormation=" + typeFormation + ", catalogue=" + catalogue + ", formateur="
				+ formateur + ", responsable=" + responsable + ", salle=" + salle + "]";
	}

	
	
}
