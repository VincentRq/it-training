package fr.ittraining.modeles.objets;

import java.time.LocalDate;

public class Formateur {
	
	private int id;
	private String nom;
	private String prenom;
	private LocalDate dateRecrutement;
	private double note;
	private String email;
	private String adresse;
	private String codePostal;
	private String ville;
	private String pays;
	private int blame;

	public Formateur() {
		this.dateRecrutement = LocalDate.now();
	}

	public Formateur(String nom, String prenom, LocalDate dateRecrutement, double note, String email, String adresse,
			String codePostal, String ville, String pays, int blame) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.dateRecrutement = LocalDate.now();
		this.note = note;
		this.email = email;
		this.adresse = adresse;
		this.codePostal = codePostal;
		this.ville = ville;
		this.pays = pays;
		this.blame = blame;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public LocalDate getDateRecrutement() {
		return dateRecrutement;
	}

	public void setDateRecrutement(LocalDate dateRecrutement) {
		this.dateRecrutement = dateRecrutement;
	}

	public double getNote() {
		return note;
	}

	public void setNote(double note) {
		this.note = note;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public int getBlame() {
		return blame;
	}

	public void setBlame(int blame) {
		this.blame = blame;
	}

	@Override
	public String toString() {
		return "Formateur [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", dateRecrutement=" + dateRecrutement
				+ ", note=" + note + ", email=" + email + ", adresse=" + adresse + ", codePostal=" + codePostal
				+ ", ville=" + ville + ", pays=" + pays + ", blame=" + blame + "]";
	}
	
}
