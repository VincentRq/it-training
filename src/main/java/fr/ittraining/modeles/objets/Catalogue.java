package fr.ittraining.modeles.objets;

public class Catalogue {
	private int id;
	private String titre;
	private String domaine;
	private String theme;
	private String sousTheme;
	private int dureeH;
	private double prix;
	private int nbStagiaireMin;
	private int nbStagiaireMax;
	private String prerequis;
	private String description;
	private String programme;
	
	public Catalogue() {
		super();
	}
	
	public Catalogue(String titre, String domaine, String theme, String sousTheme, int dureeH,
			double prix, int nbStagiaireMin, int nbStagiaireMax, String prerequis, String description, String programme) {
		super();
		this.titre = titre;
		this.domaine = domaine;
		this.theme = theme;
		this.sousTheme = sousTheme;
		this.dureeH = dureeH;
		this.prix = prix;
		this.nbStagiaireMin = nbStagiaireMin;
		this.nbStagiaireMax = nbStagiaireMax;
		this.prerequis = prerequis;
		this.description = description;
		this.programme = programme;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getDomaine() {
		return domaine;
	}
	public void setDomaine(String domaine) {
		this.domaine = domaine;
	}
	public String getTheme() {
		return theme;
	}
	public void setTheme(String theme) {
		this.theme = theme;
	}
	public String getSousTheme() {
		return sousTheme;
	}
	public void setSousTheme(String sousTheme) {
		this.sousTheme = sousTheme;
	}
	public int getDureeH() {
		return dureeH;
	}
	public void setDureeH(int dureeH) {
		this.dureeH = dureeH;
	}
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	public int getNbStagiaireMin() {
		return nbStagiaireMin;
	}
	public void setNbStagiaireMin(int nbStagiaireMin) {
		this.nbStagiaireMin = nbStagiaireMin;
	}
	public int getNbStagiaireMax() {
		return nbStagiaireMax;
	}
	public void setNbStagiaireMax(int nbStagiaireMax) {
		this.nbStagiaireMax = nbStagiaireMax;
	}
	public String getPrerequis() {
		return prerequis;
	}
	public void setPrerequis(String prerequis) {
		this.prerequis = prerequis;
	}
		
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProgramme() {
		return programme;
	}

	public void setProgramme(String programme) {
		this.programme = programme;
	}

	@Override
	public String toString() {
		return "Catalogue [id=" + id + ", titre=" + titre + ", domaine=" + domaine + ", theme=" + theme + ", sousTheme="
				+ sousTheme + ", dureeH=" + dureeH + ", prix=" + prix + ", nbStagiaireMin=" + nbStagiaireMin
				+ ", nbStagiaireMax=" + nbStagiaireMax + ", prerequis=" + prerequis + ", description=" + description
				+ ", programme=" + programme + "]";
	}
	
}
