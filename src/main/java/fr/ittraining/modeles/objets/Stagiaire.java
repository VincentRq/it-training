package fr.ittraining.modeles.objets;

public class Stagiaire {
	
	int id;
	Utilisateur codeUtilisateur;
	String nom;
	String prenom;
	String adressePostale;
	String codePostal;
	String ville;
	String pays;
	
	public Stagiaire() {
		// TODO Auto-generated constructor stub
	}

	public Stagiaire(Utilisateur codeUtilisateur, String nom, String prenom, String adressePostale, String codePostal,
			String ville, String pays) {
		super();
		this.codeUtilisateur = codeUtilisateur;
		this.nom = nom;
		this.prenom = prenom;
		this.adressePostale = adressePostale;
		this.codePostal = codePostal;
		this.ville = ville;
		this.pays = pays;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Utilisateur getCodeUtilisateur() {
		return codeUtilisateur;
	}

	public void setCodeUtilisateur(Utilisateur codeUtilisateur) {
		this.codeUtilisateur = codeUtilisateur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdressePostale() {
		return adressePostale;
	}

	public void setAdressePostale(String adressePostale) {
		this.adressePostale = adressePostale;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	
	
	@Override
	public String toString() {
		return "Stagiaire [id=" + id + ", codeUtilisateur=" + codeUtilisateur + ", nom=" + nom + ", prenom=" + prenom
				+ ", adressePostale=" + adressePostale + ", codePostal=" + codePostal + ", ville=" + ville + ", pays="
				+ pays + "]";
	}

	

	
	
	
}
	