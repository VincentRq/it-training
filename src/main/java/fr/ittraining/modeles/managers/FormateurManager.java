package fr.ittraining.modeles.managers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.ittraining.modeles.DAO.FormateurDAO;
import fr.ittraining.modeles.DAO.SalleDAO;
import fr.ittraining.modeles.objets.Formateur;

public class FormateurManager {
	
	private Map<String, String> erreurs = new HashMap<>();
	
	public Map<String, String> verif(String nom, String prenom, double note, String email, String adresse, String codePostal, String ville, String pays){
		verifNom(nom);
		verifPrenom(prenom);
		verifNote(note);
		verifEmail(email);
		verifAdresse(adresse);
		verifCodePostal(codePostal);
		verifVille(ville);
		verifPays(pays);
		
		return erreurs;
	}

	private void verifNom(String nom) {
		if(nom == "") {
			erreurs.put("nom", "Le formateur doit obligatoirement avoir un nom");
		}
	}
	
	private void verifPrenom(String prenom) {
		if(prenom == "") {
			erreurs.put("prenom", "Le formateur doit obligatoirement avoir un prenom");
		}
	}
	private void verifNote(double note) {
		if(note > 5 || note < 0) {
			erreurs.put("note", "La note du formateur doit �tre entre 0 et 5");
		}
	}
	
	private void verifEmail(String email) {
		if(!email.contains("@")) {
			erreurs.put("email", "L'adresse email du formateur doit contenir un @");
		}
	}
	
	private void verifAdresse(String adresse) {
	}
	
	private void verifCodePostal(String codePostal) {
		if(codePostal.length() > 20) {
			erreurs.put("codePostal", "Le code postal ne peut pas contenir plus de 20 caract�res");
		}
	}
	
	private void verifVille(String ville) {
	}
	
	private void verifPays(String pays) {
	}
	
	public void enregistrer(Formateur formateur) {
		new FormateurDAO().enregistrer(formateur);
	}
	
	public List<Formateur> findAll() {
		return new FormateurDAO().findAll();
	}

	public Formateur findById(int formateurId) {
		return new FormateurDAO().findById(formateurId);
	}

	public void modifier(Formateur formateur, int id) {
		new FormateurDAO().modifier(formateur, id);
	}
	
	public void supprimer(int id) {
		new FormateurDAO().supprimer(id);
	}
}
