package fr.ittraining.modeles.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import fr.ittraining.modeles.DAO.CatalogueDAO;
import fr.ittraining.modeles.objets.Catalogue;

public class CatalogueManager {
	
	private Map<String,String> erreurs = new HashMap<>();
	
	public Map<String, String> verif(String titre, String domaine, String theme, String sousTheme, int dureeH, double prix, int nbStagiaireMin, int nbStagiaireMax, String prerequis){
		verifTitre(titre);
		verifDomaine(domaine);
		verifTheme(theme);
		verifSousTheme(sousTheme);
		verifDureeH(dureeH);
		verifPrix(prix);
		verifNbStagiaireMin(nbStagiaireMin);
		verifNbStagiaireMax(nbStagiaireMax);
		verifPrerequis(prerequis);
		return erreurs;
	}

	private void verifTitre(String titre) {
		if(titre.equals("")) {
			erreurs.put("titre", "Le mod�le de formation doit obligatoirement avoir un titre");
		}
	}

	private void verifDomaine(String domaine) {
		if(domaine.equals("")) {
			erreurs.put("domaine", "Le mod�le de formation doit obligatoirement avoir un nom de domaine");
		}
	}

	private void verifTheme(String theme) {
		if(theme.equals("")) {
			erreurs.put("theme", "Le mod�le de formation doit obligatoirement avoir un th�me");
		}
	}

	private void verifSousTheme(String sousTheme) {
	}

	private void verifDureeH(int dureeH) {
		if(dureeH < 1) {
			erreurs.put("dureeH", "La dur�e de la formation ne peut-�tre inf�rieure � 1 heure");
		}
	}

	private void verifPrix(double prix) {
		if(prix < 1) {
			erreurs.put("prix", "Le prix de la formation ne peut-�tre inf�rieure � 1�");
		}
	}

	private void verifNbStagiaireMin(int nbStagiaireMin) {
		if(nbStagiaireMin < 3) {
			erreurs.put("nbStagiaireMin", "Le nombre minimum de stagiaire ne peut pas �tre inf�rieur � 3");
		}
	}

	private void verifNbStagiaireMax(int nbStagiaireMax) {
		if(nbStagiaireMax > 30) {
			erreurs.put("nbStagiaireMax", "Le nombre de stagiaire maximum ne peut pas exc�der 30");
		}
	}

	private void verifPrerequis(String prerequis) {
	}
	
	public void enregistrer(Catalogue catalogue) {
		new CatalogueDAO().enregistrer(catalogue);
	}

	public List<Catalogue> findAll() {
		return new CatalogueDAO().findAll();
	}

	public Catalogue findById(int id) {
		return new CatalogueDAO().findById(id);
	}

	public void modifier(Catalogue catalogue, int id) {
		new CatalogueDAO().modifier(catalogue, id);
	}

	public void supprimer(int id) {
		new CatalogueDAO().supprimer(id);
	}

	public List<Catalogue> findByDomaine(String domaine) {
		return new CatalogueDAO().findByDomaine(domaine);
	}
	
	public List<Catalogue> selectionnerParPertinence(String recherche) {
		List<Catalogue> catalogues = findAll();
		List <Integer> idListe = new ArrayList<>();
		recherche = StringUtils.stripAccents(recherche.toLowerCase());
		idListe = rechercheDomaine(catalogues, idListe, recherche);
		idListe = rechercheTheme(catalogues, idListe, recherche);
		idListe = rechercheSousTheme(catalogues, idListe, recherche);
		idListe = rechercheTitre(catalogues, idListe, recherche);
		idListe = rechercheDescription(catalogues, idListe, recherche);
		idListe = rechercheProgramme(catalogues, idListe, recherche);
		catalogues.clear();
		for (int id : idListe) {
			catalogues.add(findById(id));
		}
		return catalogues;
	}
	
	private List<Integer> rechercheProgramme(List<Catalogue> catalogues, List<Integer> idListe, String recherche) {
		for (Catalogue catalogue : catalogues) {
			if (catalogue.getProgramme() != null) {
				if (StringUtils.stripAccents(catalogue.getProgramme().toLowerCase()).contains(recherche)) {
					ajouterIdSiNonPresent(idListe, catalogue.getId());
				}
			}
		}
		return idListe;
	}

	private List<Integer> rechercheDescription(List<Catalogue> catalogues, List<Integer> idListe, String recherche) {
		for (Catalogue catalogue : catalogues) {
			if (catalogue.getDescription() != null) {
				if (StringUtils.stripAccents(catalogue.getDescription().toLowerCase()).contains(recherche)) {
				ajouterIdSiNonPresent(idListe, catalogue.getId());
				}
			}
		}
		return idListe;
	}

	private List<Integer> rechercheTitre(List<Catalogue> catalogues, List<Integer> idListe, String recherche) {
		for (Catalogue catalogue : catalogues) {
			if (catalogue.getTitre() != null) {
				if (StringUtils.stripAccents(catalogue.getTitre().toLowerCase()).contains(recherche)) {
					ajouterIdSiNonPresent(idListe, catalogue.getId());
				}
			}
		}
		return idListe;
	}

	private List<Integer> rechercheSousTheme(List<Catalogue> catalogues, List<Integer> idListe, String recherche) {
		for (Catalogue catalogue : catalogues) {
			if (catalogue.getSousTheme() != null) {
				if (StringUtils.stripAccents(catalogue.getSousTheme().toLowerCase()).contains(recherche)) {
					ajouterIdSiNonPresent(idListe, catalogue.getId());
				}
			}
		}
		return idListe;
	}

	private List<Integer> rechercheTheme(List<Catalogue> catalogues, List<Integer> idListe, String recherche) {
		for (Catalogue catalogue : catalogues) {
			if (catalogue.getTheme() != null) {
				if (StringUtils.stripAccents(catalogue.getTheme().toLowerCase()).contains(recherche)) {
					ajouterIdSiNonPresent(idListe, catalogue.getId());
				}
			}
		}
		return idListe;
	}

	private List<Integer> rechercheDomaine(List<Catalogue> catalogues, List<Integer> idListe, String recherche) {
		for (Catalogue catalogue : catalogues) {
			if (catalogue.getDomaine() != null) {
				if (StringUtils.stripAccents(catalogue.getDomaine().toLowerCase()).contains(recherche)) {
					ajouterIdSiNonPresent(idListe, catalogue.getId());
				}
			}
		}
		return idListe;
	}
	
	private List<Integer> ajouterIdSiNonPresent(List<Integer> idListe, int id) {
		if (!idListe.contains(id)) {
			idListe.add(id);
		}
		return idListe;
	}
}
