package fr.ittraining.modeles.managers;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.ittraining.modeles.DAO.SalleDAO;
import fr.ittraining.modeles.DAO.SessionDAO;
import fr.ittraining.modeles.objets.Catalogue;
import fr.ittraining.modeles.objets.Formateur;
import fr.ittraining.modeles.objets.Salle;
import fr.ittraining.modeles.objets.Session;
import fr.ittraining.modeles.objets.Utilisateur;

public class SessionManager {
	
	private Map<String, String> erreurs = new HashMap<>();
	
	public Map<String, String> verif(Catalogue catalogue, Formateur formateur, Utilisateur responsable, Salle salle, LocalDate dateDebut, LocalDate dateFin, int nbStagiaire, String typeFormation){
		verifCatalogue(catalogue);
		verifFormateur(formateur);
		verifResponsable(responsable);
		verifSalle(salle);
		verifDateDebut(dateDebut);
		verifDateFin(dateFin);
		verifnbStagiaire(nbStagiaire);
		verifTypeFormation(typeFormation);
		
		return erreurs;
	}

	private void verifCatalogue(Catalogue catalogue) {
	}
	private void verifFormateur(Formateur formateur) {
	}
	private void verifResponsable(Utilisateur responsable) {
	}
	private void verifSalle(Salle salle) {
	}
	private void verifDateDebut(LocalDate dateDebut) {
	}
	private void verifDateFin(LocalDate dateFin) {
	}
	private void verifnbStagiaire(int nbStagiaire) {
	}
	private void verifTypeFormation(String typeFormation) {
	}
	
	public void enregistrer(Session session) {
		new SessionDAO().enregistrer(session);
	}
	
	public List<Session> findAll() {
		return new SessionDAO().findAll();
	}

	public Session findById(int sessionId) {
		return new SessionDAO().findById(sessionId);
	}

	public void modifier(Session session, int id) {
		new SessionDAO().modifier(session, id);
	}
	
	public void supprimer(int id) {
		new SessionDAO().supprimer(id);
	}

	public List<Session> findSessionbyIdCatalogue(int id) {
		return new SessionDAO().findSessionbyIdCatalogue(id);
	}

	public List<Session> findAllByStagiaire(int stagiaireId) {
		return new SessionDAO().findAllByStagiaire(stagiaireId);
	}

	public List<Session> findSessionbyIdCatalogueAndNotAlreadySubscribed(int idCatalogue, int idUtilisateur) {
		return new SessionDAO().findSessionbyIdCatalogueAndNotAlreadySubscribed(idCatalogue, idUtilisateur);
	}
	
}
