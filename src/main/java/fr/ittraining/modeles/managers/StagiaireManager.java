package fr.ittraining.modeles.managers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.ittraining.modeles.DAO.InscriptionSessionStagiaireDAO;
import fr.ittraining.modeles.DAO.StagiaireDAO;
import fr.ittraining.modeles.objets.Catalogue;
import fr.ittraining.modeles.objets.Stagiaire;

public class StagiaireManager {
	
	StagiaireDAO stagiaireDAO = new StagiaireDAO();
	InscriptionSessionStagiaireDAO inscriptionSessionStagiaireDAO = new InscriptionSessionStagiaireDAO();
	private Map<String, String> erreurs = new HashMap<>();

	public Map<String, String> verif(String nom, String prenom, String adressePostale,
			String codePostal, String ville, String pays) {
		verifNom(nom);
		verifPrenom(prenom);
		verifAdressePostale(adressePostale);
		verifCodePostal(codePostal);
		verifVille(ville);
		verifPays(pays);
		return erreurs;
	}
	
	private void verifNom(String nom) {
	}
	private void verifPrenom(String prenom) {
	}
	private void verifAdressePostale(String adressePostale) {
	}
	private void verifCodePostal(String codePostal) {
	}
	private void verifVille(String ville) {
	}
	private void verifPays(String pays) {
	}
	
	public int findStagiaireIdByCodeUtilisateur(int codeUtilisateur) {
			int idStagiaire = stagiaireDAO.findStagiaireIdByCodeUtilisateur(codeUtilisateur);
		return idStagiaire;
	}
	
	public Stagiaire findStagiaireByCodeUtilisateur(int codeUtilisateur) {
			Stagiaire stagiaire = stagiaireDAO.findStagiaireByCodeUtilisateur(codeUtilisateur);
		return stagiaire;
	}
	
	public List<Integer> findAllCodeUtilisateurs() {
		return new StagiaireDAO().findAllCodesUtilisateurs();
	}
	
	public void enregistrerStagiaire(Stagiaire stagiaire) {
		inscriptionSessionStagiaireDAO.enregistrerStagiaire(stagiaire);
		
	}

	public Stagiaire findById(int id) {
		return new StagiaireDAO().findById(id);
	}

	public void supprimer(int id) {
		new StagiaireDAO().supprimer(id);
	}

	public void enregistrer(Stagiaire stagiaire) {
		new StagiaireDAO().enregistrer(stagiaire);
	}

	public void modifier(Stagiaire stagiaire, int id) {
		new StagiaireDAO().modifier(stagiaire, id);
	}

	public List<Stagiaire> findAll() {
		return new StagiaireDAO().findAll();
	}
}
