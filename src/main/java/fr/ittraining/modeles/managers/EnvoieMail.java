package fr.ittraining.modeles.managers;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;



public class EnvoieMail {

	public static void sendMessage(String subject, String text, String destinataire, String copyDest) {
	    // 1 -> Cr�ation de la session
	    Properties properties = new Properties();
	    properties.setProperty("mail.transport.protocol", "smtp");
	    properties.setProperty("mail.smtp.host", "smtp.gmail.com");
	    properties.setProperty("mail.smtp.user", "it.training404@gmail.com");
	    properties.setProperty("mail.from", "it.training404@gmail.com");
	    properties.setProperty("mail.smtp.starttls.enable", "true");
	    Session session = Session.getInstance(properties);
	    
	    // 2 -> Cr�ation du message
	    MimeMessage message = new MimeMessage(session);
	    try {
	        message.setText(text);
	        message.setSubject(subject);
	        message.addRecipients(Message.RecipientType.TO, destinataire);
	        message.addRecipients(Message.RecipientType.CC, copyDest);
	    } catch (MessagingException e) {
	        e.printStackTrace();
	    }
	    
	 // 3 -> Envoi du message
	    Transport transport = null;
	    try {
	        transport = session.getTransport("smtp");
	        transport.connect("it.training404@gmail.com", "itTraining404500");
	        transport.sendMessage(message, new Address[] { new InternetAddress(destinataire),
	                                                        new InternetAddress(copyDest) });
		    } catch (MessagingException e) {
		        e.printStackTrace();
		    } finally {
		        try {
		            if (transport != null) {
		                transport.close();
		            }
		        } catch (MessagingException e) {
		            e.printStackTrace();
		        }
	    }
	}
}
