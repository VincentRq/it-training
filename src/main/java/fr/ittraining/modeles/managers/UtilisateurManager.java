package fr.ittraining.modeles.managers;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.ittraining.modeles.DAO.UtilisateurDAO;
import fr.ittraining.modeles.objets.Utilisateur;

public class UtilisateurManager {
	
	Map<String,String> erreurs = new HashMap<>();
	
	public void enregistrerUtilisateur(Utilisateur utilisateur) {
	UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
	utilisateurDAO.enregistrerUtilisateur(utilisateur);
	}

	public void modifierUtilisateur(Utilisateur utilisateur, int id) {
		new UtilisateurDAO().modifierUtilisateur(utilisateur, id);
	}
		
	public Map<String,String> verif(String email, String mdp, String verifMdp) {
		
		verifEmail(email);
		verifMdp(mdp, verifMdp);
		return erreurs;
	}
	
	public void verifEmail(String email) {
		if(!(email.trim().endsWith(".fr") || email.endsWith(".com") )) {
			erreurs.put("email", "L'email doit terminer par .com ou .fr");
		} else {
			List<String> emails = this.findAllEmail();
			if (emails.contains(email)) {
				erreurs.put("email", "Cet email est d�j� utilis�");
			}
		}
	}
	
	public void verifMdp(String mdp, String verifMdp) {
		if(!(mdp.length() > 7)) {
			erreurs.put("mdp", "Le mot de passe doit contenir au minimum 8 caract�res");
		}
		if(!mdp.equals(verifMdp)) {
			erreurs.put("verifMdp", "Les mots de passes ne sont pas identiques");
		}
	}
	
	public Utilisateur findByEmailAndMdp(String email, String mdp) {
		UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
		Utilisateur utilisateur = utilisateurDAO.findByEmailAndMdp(email, mdp);	
		
		return utilisateur;
	}
	
	public Utilisateur findById(int id) {
		UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
		Utilisateur utilisateur = utilisateurDAO.findById(id);	
		
		return utilisateur;
	}
	
	public List<Utilisateur> findAll() {
		UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
		List<Utilisateur> utilisateurs = utilisateurDAO.findAll();	
		
		return utilisateurs;
	}
	
	public List<String> findAllEmail() {
		UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
		List<String> emails = utilisateurDAO.findAllEmail();	
		
		return emails;
	}
	
	public List<Utilisateur> findAllAdmin() {
		UtilisateurDAO utilisateurDAO = new UtilisateurDAO();
		List<Utilisateur> utilisateurs = utilisateurDAO.findAllAdmin();	
		
		return utilisateurs;
	}

	public void supprimer(int id) {
		new UtilisateurDAO().supprimer(id);
		
	}
	
	public void nommerAdministrateur(int id) {
		new UtilisateurDAO().nommerAdministrateur(id);
	}

	public void destituerAdministrateur(int id) {
		new UtilisateurDAO().destituerAdministrateur(id);
	}
}
