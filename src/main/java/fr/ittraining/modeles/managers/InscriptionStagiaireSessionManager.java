package fr.ittraining.modeles.managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.ittraining.modeles.DAO.InscriptionSessionStagiaireDAO;
import fr.ittraining.modeles.DAO.PoolConnexion;
import fr.ittraining.modeles.objets.Salle;
import fr.ittraining.modeles.objets.Session;
import fr.ittraining.modeles.objets.Stagiaire;

public class InscriptionStagiaireSessionManager {

	InscriptionSessionStagiaireDAO inscriptionSessionStagiaireDAO = new InscriptionSessionStagiaireDAO();
	
	public void enregistrerStagiaire(Stagiaire stagiaire) {
		inscriptionSessionStagiaireDAO.enregistrerStagiaire(stagiaire);
		
	}

	public void enregistrerStagiaireSession(int idStagiaire, int sessionId) {
		SessionManager sessionManager = new SessionManager();
		inscriptionSessionStagiaireDAO.enregistrerSessionStagiaire(idStagiaire, sessionId);
		Session session = sessionManager.findById(sessionId);
		session.setNbStagiaire(session.getNbStagiaire() + 1);	
		if (session.getNbStagiaire() == session.getCatalogue().getNbStagiaireMin()) {
			EnvoieMail.sendMessage("Formation du " + session.getDateDebut(), "Bonjour " + session.getFormateur().getPrenom() + ", \n\n"
					+ "Votre formation [" + session.getCatalogue().getTitre() + "] est bien confirm�e pour d�buter le " + session.getDateDebut() + ".\n"
							+ "Pour tout probl�me, veuillez contacter notre responsable de formation au 0606060606.\n\n"
							+ "Bien cordialement,\nIT-Training", session.getFormateur().getEmail(), "vincent.roquet@gmail.com");
		}
		sessionManager.modifier(session, sessionId);
	}
	
	public boolean sessionStagiaireExist(int idSession, int idStagiaire) {
		return inscriptionSessionStagiaireDAO.sessionStagiaireExist(idSession,idStagiaire);
	}
}
