package fr.ittraining.modeles.managers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.ittraining.modeles.DAO.SalleDAO;
import fr.ittraining.modeles.objets.Salle;

public class SalleManager {
	
	private Map<String, String> erreurs = new HashMap<>();
	
	public Map<String, String> verif(String nom, int capacite, String adresse, String codePostal, String ville, String pays){
		verifNom(nom);
		verifCapacite(capacite);
		verifAdresse(adresse);
		verifCodePostal(codePostal);
		verifVille(ville);
		verifPays(pays);
		
		return erreurs;
	}

	private void verifNom(String nom) {
		if(nom == "") {
			erreurs.put("nom", "La salle doit obligatoirement avoir un nom");
		}
	}
	
	private void verifCapacite(int capacite) {
		if(capacite <= 3) {
			erreurs.put("capacite", "La salle ne peut pas avoir une capacit� inf�rieure � 4");
		}
	}
	
	private void verifAdresse(String adresse) {
	}
	
	private void verifCodePostal(String codePostal) {
		if(codePostal.length() > 20) {
			erreurs.put("codePostal", "Le code postal ne peut pas contenir plus de 20 caract�res");
		}
	}
	
	private void verifVille(String ville) {
	}
	
	private void verifPays(String pays) {
	}
	
	public void enregistrer(Salle salle) {
		new SalleDAO().enregistrer(salle);
	}
	
	public List<Salle> findAll() {
		return new SalleDAO().findAll();
	}

	public Salle findById(int id) {
		return new SalleDAO().findById(id);
	}

	public void modifier(Salle salle, int id) {
		new SalleDAO().modifier(salle, id);
	}

	public void supprimer(int id) {
		new SalleDAO().supprimer(id);
	}
}
