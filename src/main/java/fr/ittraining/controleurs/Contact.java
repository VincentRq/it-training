package fr.ittraining.controleurs;

import java.io.IOException;

import fr.ittraining.modeles.managers.EnvoieMail;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/contact")
public class Contact extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//EnvoieMail.sendMessage("test", "Ceci est \n un test", "vincent.roquet@gmail.com", "vincent.roquet@gmail.com");
		this.getServletContext().getRequestDispatcher("/WEB-INF/contact.jsp").forward(req, resp);
	}

}
