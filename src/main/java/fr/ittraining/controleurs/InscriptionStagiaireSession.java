package fr.ittraining.controleurs;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import fr.ittraining.modeles.managers.InscriptionStagiaireSessionManager;
import fr.ittraining.modeles.managers.SessionManager;
import fr.ittraining.modeles.managers.StagiaireManager;
import fr.ittraining.modeles.objets.Catalogue;
import fr.ittraining.modeles.objets.Session;
import fr.ittraining.modeles.objets.Stagiaire;
//import fr.ittraining.modeles.objets.Stagiaire;
import fr.ittraining.modeles.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/inscriptionStagiaireSession")
public class InscriptionStagiaireSession extends HttpServlet {

	Catalogue catalogue = new Catalogue();
	Utilisateur utilisateur = new Utilisateur();
	Session session = new Session();
	InscriptionStagiaireSessionManager inscriptionStagiaireSessionManager = new InscriptionStagiaireSessionManager();
	StagiaireManager stagiaireManager = new StagiaireManager();
	
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		catalogue = (Catalogue) request.getSession().getAttribute("catalogue");
		request.setAttribute("catalogue", catalogue);
		Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
		List<Session> sessions = new SessionManager().findSessionbyIdCatalogueAndNotAlreadySubscribed(catalogue.getId(), utilisateur.getId());
		request.setAttribute("sessions", sessions);
		int codeUtilisateur = utilisateur.getId();
		List<Integer> codesUtilisateurs = new StagiaireManager().findAllCodeUtilisateurs();
		if (codesUtilisateurs.contains(codeUtilisateur)) {
			Stagiaire stagiaire = new StagiaireManager().findStagiaireByCodeUtilisateur(codeUtilisateur);
			request.setAttribute("stagiaire", stagiaire);
		}
		LocalDate date = LocalDate.now();
		request.setAttribute("date", date);
				
		this.getServletContext().getRequestDispatcher("/WEB-INF/inscriptionStagiaireSession.jsp").forward(request, response);
	}
		
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idStagiaire;
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String adressePostale = request.getParameter("adressePostale");
		String codePostal = request.getParameter("codePostal");
		String ville = request.getParameter("ville");
		String pays = request.getParameter("pays");
		utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
		System.out.println("idStagiaire : " + request.getParameter("idStagiaire"));
		if (request.getParameter("idStagiaire") == null || request.getParameter("idStagiaire") == "") {
			Stagiaire stagiaire = new Stagiaire(utilisateur, nom, prenom, adressePostale, codePostal, ville, pays);
			inscriptionStagiaireSessionManager.enregistrerStagiaire(stagiaire);
			idStagiaire = stagiaireManager.findStagiaireIdByCodeUtilisateur(utilisateur.getId());
		} else {
			idStagiaire = Integer.valueOf(request.getParameter("idStagiaire"));
		}
		
		int idSession = Integer.parseInt(request.getParameter("sessionId"));
		if(inscriptionStagiaireSessionManager.sessionStagiaireExist(idSession, idStagiaire)) {
			request.setAttribute("messageSuccess", "Vous �tes d�j� inscrit � cette session de formation.");
		} else {
			inscriptionStagiaireSessionManager.enregistrerStagiaireSession(idStagiaire, idSession);
			request.setAttribute("messageSuccess", "Votre demande d'inscription a �t� prise en compte. Elle sera valid�e sous r�serve de r�ception de vote paiment par ch�que � l'ordre de Mr Responsable.");
		}
		this.getServletContext().getRequestDispatcher("/WEB-INF/inscriptionStagiaireSession.jsp").forward(request, response);
	}
	
}
