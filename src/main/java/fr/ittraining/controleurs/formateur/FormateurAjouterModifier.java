package fr.ittraining.controleurs.formateur;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Map;

import fr.ittraining.modeles.managers.FormateurManager;
import fr.ittraining.modeles.objets.Formateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/formateurAjouterModifier")
public class FormateurAjouterModifier extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/formateur/formateurAjouterModifier.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Boolean formulairePasse;
		if(request.getParameter("formulairePasse") != null) {
			formulairePasse = Boolean.valueOf(request.getParameter("formulairePasse"));
		} else {
			formulairePasse = false;
		}
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		LocalDate dateRecrutement = LocalDate.parse(request.getParameter("dateRecrutement"));
		double note = 0.0;
		if (request.getParameter("note") != null) {
			note = Double.valueOf(request.getParameter("note"));
		}
		String email = request.getParameter("email");
		String adresse = request.getParameter("adresse");
		String codePostal = request.getParameter("codePostal");
		String ville = request.getParameter("ville");
		String pays = request.getParameter("pays");
		int blame = 0;
		String blameString = request.getParameter("blame");
		if (blameString != null && blameString != "") {
			blame = Integer.valueOf(blameString);
		}
		
		FormateurManager formateurManager = new FormateurManager();
		
		Map<String, String> erreurs = formateurManager.verif(nom, prenom, note, email, adresse, codePostal, ville, pays);
		
		if(formulairePasse) {
			if(erreurs.isEmpty()) { // Formulaire pass� sans erreur --> On cr�e la formateur
				Formateur formateur = new Formateur(nom, prenom, dateRecrutement, note, email, adresse, codePostal, ville, pays, blame);
				if(request.getParameter("id") == null) { 	// Il s'agit d'une cr�ation --> On ajoute une ligne � la BDD
					formateurManager.enregistrer(formateur);
					request.setAttribute("messageSucces", "Le.a nouveau.lle formateur.rice a bien �t� cr��.e");
				} else {									// Il s'agit d'une modification --> On modifie une ligne de la BDD
					int id = Integer.valueOf(request.getParameter("id"));
					formateurManager.modifier(formateur, id);
					request.setAttribute("messageSucces", "Le.a nouveau.lle formateur.rice a bien �t� modifi�.e");
				}
				response.sendRedirect(request.getContextPath() + "/formateurListe");
			} else {				// Formulaire pass� avec erreur --> on renvoie au formulaire avec les erreurs
				request.setAttribute("erreurs", erreurs);
				this.getServletContext().getRequestDispatcher("/WEB-INF/formateur/formateurAjouterModifier.jsp").forward(request, response);
			}
		} else if (!formulairePasse) { // Formulaire non pass� malgr� requ�te POST --> envoie vers le formulaire pour modifier les champs
			this.getServletContext().getRequestDispatcher("/WEB-INF/formateur/formateurAjouterModifier.jsp").forward(request, response);
		}
	}
}
