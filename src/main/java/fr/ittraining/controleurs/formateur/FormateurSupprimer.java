package fr.ittraining.controleurs.formateur;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import fr.ittraining.modeles.managers.FormateurManager;
import fr.ittraining.modeles.objets.Formateur;

@WebServlet(urlPatterns="/formateurSupprimer")
public class FormateurSupprimer extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FormateurManager formateurManager = new FormateurManager();
		int id = Integer.valueOf(request.getParameter("id"));
		Formateur formateur = formateurManager.findById(id);
		request.setAttribute("messageSucces", "La formateur " + formateur.getNom() + " a �t� supprim�e");
		formateurManager.supprimer(id);
		response.sendRedirect(request.getContextPath() + "/formateurListe");
	}
}
