package fr.ittraining.controleurs.formateur;

import java.io.IOException;

import fr.ittraining.modeles.managers.FormateurManager;
import fr.ittraining.modeles.objets.Formateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/formateurDetail")
public class FormateurDetail extends HttpServlet{

	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Formateur formateur = new FormateurManager().findById(Integer.valueOf(request.getParameter("formateurId")));
		request.setAttribute("formateur", formateur);
		this.getServletContext().getRequestDispatcher("/WEB-INF/formateur/formateurDetail.jsp").forward(request, response);
	}
}

