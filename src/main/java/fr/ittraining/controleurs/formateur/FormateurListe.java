package fr.ittraining.controleurs.formateur;

import java.io.IOException;
import java.util.List;

import fr.ittraining.modeles.managers.FormateurManager;
import fr.ittraining.modeles.objets.Formateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/formateurListe")
public class FormateurListe extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Formateur> formateurs = new FormateurManager().findAll();
		request.setAttribute("formateurs", formateurs);
		this.getServletContext().getRequestDispatcher("/WEB-INF/formateur/formateurListe.jsp").forward(request, response);
	}
}
