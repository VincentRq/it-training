package fr.ittraining.controleurs.formationPage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.ittraining.modeles.managers.CatalogueManager;
import fr.ittraining.modeles.objets.Catalogue;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/formationRessourcesHumaines")
public class formationRessourcesHumaines extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Catalogue> catalogues = new CatalogueManager().findByDomaine("Ressources humaines");
		request.setAttribute("catalogues", catalogues);
		List<String> themes = new ArrayList<>();
		for (Catalogue catalogue : catalogues) {
			if(!themes.contains(catalogue.getTheme())) {
				themes.add(catalogue.getTheme());
			}
		}
		request.setAttribute("themes", themes);
		this.getServletContext().getRequestDispatcher("/WEB-INF/formationPages/formationRessourcesHumaines.jsp").forward(request, response);
	}
}
