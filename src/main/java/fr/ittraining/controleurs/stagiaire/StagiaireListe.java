package fr.ittraining.controleurs.stagiaire;

import java.io.IOException;
import java.util.List;

import fr.ittraining.modeles.managers.StagiaireManager;
import fr.ittraining.modeles.objets.Stagiaire;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/stagiaireListe")
public class StagiaireListe extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Stagiaire> stagiaires = new StagiaireManager().findAll();
		request.setAttribute("stagiaires", stagiaires);
		this.getServletContext().getRequestDispatcher("/WEB-INF/stagiaire/stagiaireListe.jsp").forward(request, response);
	}
}
