package fr.ittraining.controleurs.stagiaire;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import fr.ittraining.modeles.managers.StagiaireManager;
import fr.ittraining.modeles.objets.Stagiaire;

@WebServlet(urlPatterns="/stagiaireSupprimer")
public class StagiaireSupprimer extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StagiaireManager stagiaireManager = new StagiaireManager();
		int id = Integer.valueOf(request.getParameter("id"));
		Stagiaire stagiaire = stagiaireManager.findById(id);
		request.setAttribute("messageSucces", "La stagiaire " + stagiaire.getNom() + " a �t� supprim�e");
		stagiaireManager.supprimer(id);
		response.sendRedirect(request.getContextPath() + "/stagiaireListe");
	}
}
