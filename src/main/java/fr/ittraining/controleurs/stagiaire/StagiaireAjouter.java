package fr.ittraining.controleurs.stagiaire;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import fr.ittraining.modeles.managers.StagiaireManager;
import fr.ittraining.modeles.managers.UtilisateurManager;
import fr.ittraining.modeles.objets.Stagiaire;
import fr.ittraining.modeles.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/stagiaireAjouter")
public class StagiaireAjouter extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Utilisateur> utilisateurs = new UtilisateurManager().findAll();
		request.setAttribute("utilisateurs", utilisateurs);
		this.getServletContext().getRequestDispatcher("/WEB-INF/stagiaire/stagiaireAjouter.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Utilisateur> utilisateurs = new UtilisateurManager().findAll();
		request.setAttribute("utilisateurs", utilisateurs);

		int utilisateurId;
		Utilisateur utilisateur = null;
		if (request.getParameter("utilisateurId") != null) {
			utilisateurId = Integer.valueOf(request.getParameter("utilisateurId"));
			utilisateur = new UtilisateurManager().findById(utilisateurId);
			request.setAttribute("utilisateur", utilisateur);
		}
		
		Boolean formulairePasse;
		if(request.getParameter("formulairePasse") != null) {
			formulairePasse = Boolean.valueOf(request.getParameter("formulairePasse"));
			request.setAttribute("formulairePasse", formulairePasse);
		} else {
			formulairePasse = false;
		}
		
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String adressePostale = request.getParameter("adressePostale");
		String codePostal = request.getParameter("codePostal");
		String ville = request.getParameter("ville");
		String pays = request.getParameter("pays");
		
		StagiaireManager stagiaireManager = new StagiaireManager();
		
		Map<String, String> erreurs = stagiaireManager.verif(nom, prenom, adressePostale, codePostal, ville, pays);
		
		if(formulairePasse) {
			if(erreurs.isEmpty()) { // Formulaire pass� sans erreur --> On cr�e la stagiaire
				Stagiaire stagiaire = new Stagiaire(utilisateur, nom, prenom, adressePostale, codePostal, ville, pays);
				stagiaireManager.enregistrer(stagiaire);
				request.setAttribute("messageSucces", "Le.a nouveau.lle stagiaire.rice a bien �t� cr��.e");
				response.sendRedirect(request.getContextPath() + "/stagiaireListe");
			} else {				// Formulaire pass� avec erreur --> on renvoie au formulaire avec les erreurs
				request.setAttribute("erreurs", erreurs);
				this.getServletContext().getRequestDispatcher("/WEB-INF/stagiaire/stagiaireAjouter.jsp").forward(request, response);
			}
		} else if (!formulairePasse) { // Formulaire non pass� malgr� requ�te POST --> envoie vers le formulaire pour modifier les champs
			this.getServletContext().getRequestDispatcher("/WEB-INF/stagiaire/stagiaireAjouter.jsp").forward(request, response);
		}
	}
}
