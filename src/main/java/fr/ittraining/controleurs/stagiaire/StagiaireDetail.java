package fr.ittraining.controleurs.stagiaire;

import java.io.IOException;

import fr.ittraining.modeles.managers.StagiaireManager;
import fr.ittraining.modeles.objets.Stagiaire;
import fr.ittraining.modeles.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/stagiaireDetail")
public class StagiaireDetail extends HttpServlet{
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Stagiaire stagiaire = new StagiaireManager().findById(Integer.valueOf(request.getParameter("stagiaireId")));
		request.setAttribute("stagiaire", stagiaire);
		Utilisateur utilisateurAssocie = stagiaire.getCodeUtilisateur();
		request.setAttribute("utilisateurAssocie", utilisateurAssocie);
		this.getServletContext().getRequestDispatcher("/WEB-INF/stagiaire/stagiaireDetail.jsp").forward(request, response);
	}
}

