package fr.ittraining.controleurs.utilisateur;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import fr.ittraining.modeles.managers.UtilisateurManager;
import fr.ittraining.modeles.objets.Utilisateur;

@WebServlet(urlPatterns="/utilisateurSupprimer")
public class UtilisateurSupprimer extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UtilisateurManager utilisateurManager = new UtilisateurManager();
		int id = Integer.valueOf(request.getParameter("id"));
		Utilisateur utilisateur = utilisateurManager.findById(id);
		request.setAttribute("messageSucces", "L'utilisateur " + utilisateur.getEmail() + " a �t� supprim�e");
		utilisateurManager.supprimer(id);
		response.sendRedirect(request.getContextPath() + "/utilisateurListe");
	}
}
