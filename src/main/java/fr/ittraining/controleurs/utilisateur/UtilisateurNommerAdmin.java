package fr.ittraining.controleurs.utilisateur;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import fr.ittraining.modeles.managers.UtilisateurManager;
import fr.ittraining.modeles.objets.Utilisateur;

@WebServlet(urlPatterns="/utilisateurNommerAdmin")
public class UtilisateurNommerAdmin extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.valueOf(request.getParameter("id"));
		new UtilisateurManager().nommerAdministrateur(id);
		Utilisateur utilisateur = new UtilisateurManager().findById(id);
		request.setAttribute("messageSucces", "L'utilisateur " + utilisateur.getEmail() + " a �t� nomm� administrateur");
		this.getServletContext().getRequestDispatcher("/utilisateurDetail").forward(request, response);
	}
}
