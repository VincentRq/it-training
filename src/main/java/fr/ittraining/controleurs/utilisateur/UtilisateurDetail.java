package fr.ittraining.controleurs.utilisateur;

import java.io.IOException;
import java.util.List;

import fr.ittraining.modeles.managers.FormateurManager;
import fr.ittraining.modeles.managers.StagiaireManager;
import fr.ittraining.modeles.managers.UtilisateurManager;
import fr.ittraining.modeles.objets.Formateur;
import fr.ittraining.modeles.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/utilisateurDetail")
public class UtilisateurDetail extends HttpServlet{

	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Utilisateur utilisateur = new UtilisateurManager().findById(Integer.valueOf(request.getParameter("id")));
		request.setAttribute("utilisateur", utilisateur);
		int stagiaireId = new StagiaireManager().findStagiaireIdByCodeUtilisateur(utilisateur.getId());
		boolean stagiaireExists;
		if (stagiaireId != 0) {
			stagiaireExists = true;
		} else {
			stagiaireExists = false;
		}
		request.setAttribute("stagiaireExists", stagiaireExists);
		this.getServletContext().getRequestDispatcher("/WEB-INF/utilisateur/utilisateurDetail.jsp").forward(request, response);
	}
}

