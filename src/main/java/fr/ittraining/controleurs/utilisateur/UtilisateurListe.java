package fr.ittraining.controleurs.utilisateur;

import java.io.IOException;
import java.util.List;

import fr.ittraining.modeles.managers.UtilisateurManager;
import fr.ittraining.modeles.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/utilisateurListe")
public class UtilisateurListe extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Utilisateur> utilisateurs = new UtilisateurManager().findAll();
		request.setAttribute("utilisateurs", utilisateurs);
		this.getServletContext().getRequestDispatcher("/WEB-INF/utilisateur/utilisateurListe.jsp").forward(request, response);
	}
}
