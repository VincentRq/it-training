package fr.ittraining.controleurs.utilisateur;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import fr.ittraining.modeles.managers.UtilisateurManager;
import fr.ittraining.modeles.objets.Utilisateur;

@WebServlet(urlPatterns="/utilisateurDestituerAdmin")
public class UtilisateurDestituerAdmin extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.valueOf(request.getParameter("id"));
		new UtilisateurManager().destituerAdministrateur(id);
		Utilisateur utilisateur = new UtilisateurManager().findById(id);
		request.setAttribute("messageSucces", "L'utilisateur " + utilisateur.getEmail() + " a perdu ses droits d'administrateur");
		this.getServletContext().getRequestDispatcher("/utilisateurDetail").forward(request, response);
	}
}
