package fr.ittraining.controleurs.utilisateur;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import fr.ittraining.modeles.managers.UtilisateurManager;
import fr.ittraining.modeles.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/utilisateurAjouterModifier")
public class UtilisateurAjouterModifier extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/utilisateur/utilisateurAjouterModifier.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Boolean formulairePasse;
		if(request.getParameter("formulairePasse") != null) {
			formulairePasse = Boolean.valueOf(request.getParameter("formulairePasse"));
		} else {
			formulairePasse = false;
		}
		String email = request.getParameter("email");
		String entreprise = request.getParameter("entreprise");
		String mdp = request.getParameter("mdp");
		String verifMdp = request.getParameter("verifMdp");
		boolean admin = Boolean.valueOf(request.getParameter("admin"));
		
		UtilisateurManager utilisateurManager = new UtilisateurManager();
		
		Map<String, String> erreurs = utilisateurManager.verif(email, mdp, verifMdp);
		
		if(formulairePasse) {
			if(erreurs.isEmpty()) { // Formulaire pass� sans erreur --> On cr�e l'utilisateur
				Utilisateur utilisateur = new Utilisateur(email, entreprise, mdp, admin);
				if(request.getParameter("id") == null) { 	// Il s'agit d'une cr�ation --> On ajoute une ligne � la BDD
					System.out.println("utilisateur : " + utilisateur);
					utilisateurManager.enregistrerUtilisateur(utilisateur);
					request.setAttribute("messageSucces", "Le nouvel utilisateur a bien �t� cr��e");
				} else {									// Il s'agit d'une modification --> On modifie une ligne de la BDD
					int id = Integer.valueOf(request.getParameter("id"));
					utilisateurManager.modifierUtilisateur(utilisateur, id);
					request.setAttribute("messageSucces", "L'utilisateur a bien �t� modifi�e");
				}
				response.sendRedirect(request.getContextPath() + "/utilisateurListe");
			} else {				// Formulaire pass� avec erreur --> on renvoie au formulaire avec les erreurs
				request.setAttribute("erreurs", erreurs);
				this.getServletContext().getRequestDispatcher("/WEB-INF/utilisateur/utilisateurAjouterModifier.jsp").forward(request, response);
			}
		} else if (!formulairePasse) { // Formulaire non pass� malgr� requ�te POST --> envoie vers le formulaire pour modifier les champs
			this.getServletContext().getRequestDispatcher("/WEB-INF/utilisateur/utilisateurAjouterModifier.jsp").forward(request, response);
		}
	}
}
