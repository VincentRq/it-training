package fr.ittraining.controleurs.session;

import java.io.IOException;
import java.util.List;

import fr.ittraining.modeles.managers.SessionManager;
import fr.ittraining.modeles.objets.Session;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/sessionListe")
public class SessionListe extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Session> sessions = new SessionManager().findAll();
		request.setAttribute("sessions", sessions);
		this.getServletContext().getRequestDispatcher("/WEB-INF/session/sessionListe.jsp").forward(request, response);
	}
}
