package fr.ittraining.controleurs.session;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import fr.ittraining.modeles.managers.CatalogueManager;
import fr.ittraining.modeles.managers.FormateurManager;
import fr.ittraining.modeles.managers.SalleManager;
import fr.ittraining.modeles.managers.SessionManager;
import fr.ittraining.modeles.managers.UtilisateurManager;
import fr.ittraining.modeles.objets.Catalogue;
import fr.ittraining.modeles.objets.Formateur;
import fr.ittraining.modeles.objets.Salle;
import fr.ittraining.modeles.objets.Session;
import fr.ittraining.modeles.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/sessionModifier")
public class SessionModifier extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Salle> salles = new SalleManager().findAll();
		request.setAttribute("salles", salles);
		List<Formateur> formateurs = new FormateurManager().findAll();
		request.setAttribute("formateurs", formateurs);
		List<Catalogue> catalogues = new CatalogueManager().findAll();
		request.setAttribute("catalogues", catalogues);
		List<Utilisateur> responsables = new UtilisateurManager().findAllAdmin();
		request.setAttribute("responsables", responsables);
		int sessionId = Integer.valueOf(request.getParameter("id"));
		Session session = new SessionManager().findById(sessionId);
		request.setAttribute("session", session);
		this.getServletContext().getRequestDispatcher("/WEB-INF/session/sessionModifier.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		int catalogueId = Integer.valueOf(request.getParameter("catalogueId"));
		Catalogue catalogue = new CatalogueManager().findById(catalogueId);
		int formateurId = Integer.valueOf(request.getParameter("formateurId"));
		Formateur formateur = new FormateurManager().findById(formateurId);
		int salleId = Integer.valueOf(request.getParameter("salleId"));
		Salle salle = new SalleManager().findById(salleId);
		int responsableId = Integer.valueOf(request.getParameter("responsableId"));
		Utilisateur responsable = new UtilisateurManager().findById(responsableId);
		LocalDate dateDebut = LocalDate.parse(request.getParameter("dateDebut"));
		LocalDate dateFin = LocalDate.parse(request.getParameter("dateFin"));
		int nbStagiaire = Integer.valueOf(request.getParameter("nbStagiaire"));;
		String typeFormation = request.getParameter("typeFormation");
		int sessionId = Integer.valueOf(request.getParameter("id"));
		
		SessionManager sessionManager = new SessionManager();
		
		Map<String, String> erreurs = sessionManager.verif(catalogue, formateur, responsable, salle, dateDebut, dateFin, nbStagiaire, typeFormation);
		Session session = new Session(dateDebut, dateFin, nbStagiaire, typeFormation, catalogue, formateur, responsable, salle);
		sessionManager.modifier(session, sessionId);
		request.setAttribute("messageSucces", "La nouvelle session a bien �t� cr��e");
		response.sendRedirect(request.getContextPath() + "/sessionListe");
	}

}
