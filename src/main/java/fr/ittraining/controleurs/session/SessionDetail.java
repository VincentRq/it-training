package fr.ittraining.controleurs.session;

import java.io.IOException;

import fr.ittraining.modeles.managers.SessionManager;
import fr.ittraining.modeles.objets.Session;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/sessionDetail")
public class SessionDetail extends HttpServlet{

	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Session session = new SessionManager().findById(Integer.valueOf(request.getParameter("sessionId")));
		request.setAttribute("session", session);
		this.getServletContext().getRequestDispatcher("/WEB-INF/session/sessionDetail.jsp").forward(request, response);
	}
}

