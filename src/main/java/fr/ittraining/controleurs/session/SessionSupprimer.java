package fr.ittraining.controleurs.session;

import java.io.IOException;

import fr.ittraining.modeles.managers.SessionManager;
import fr.ittraining.modeles.objets.Session;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/sessionSupprimer")
public class SessionSupprimer extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SessionManager sessionManager = new SessionManager();
		int id = Integer.valueOf(request.getParameter("id"));
		Session session = sessionManager.findById(id);
		request.setAttribute("messageSucces", "La session #" + session.getId() + " a �t� supprim�e");
		sessionManager.supprimer(id);
		response.sendRedirect(request.getContextPath() + "/sessionListe");
	}
}
