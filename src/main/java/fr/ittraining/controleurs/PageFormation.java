package fr.ittraining.controleurs;

import java.io.IOException;

import com.oracle.wls.shaded.org.apache.xalan.templates.ElemValueOf;

import fr.ittraining.modeles.managers.CatalogueManager;
import fr.ittraining.modeles.objets.Catalogue;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/pageFormation")
public class PageFormation extends HttpServlet {

	Catalogue catalogue = new Catalogue();
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/pageFormation.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		CatalogueManager catalogueManager = new CatalogueManager();
		catalogue = catalogueManager.findById(id);
		request.setAttribute("catalogue", catalogue);
		request.getSession().setAttribute("catalogue", catalogue);
		this.getServletContext().getRequestDispatcher("/WEB-INF/pageFormation.jsp").forward(request, response);
	}
	
}
