package fr.ittraining.controleurs;

import java.io.IOException;
import java.util.List;

import fr.ittraining.modeles.managers.SessionManager;
import fr.ittraining.modeles.managers.StagiaireManager;
import fr.ittraining.modeles.objets.Session;
import fr.ittraining.modeles.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/tableauDeBordUtilisateur")
public class TableauDeBordUtilisateur extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
		System.out.println(utilisateur);
		int idStagiaire = new StagiaireManager().findStagiaireIdByCodeUtilisateur(utilisateur.getId());
		List<Session> sessions = new SessionManager().findAllByStagiaire(idStagiaire);
		request.setAttribute("sessions", sessions);
		this.getServletContext().getRequestDispatcher("/WEB-INF/tableauDeBordUtilisateur.jsp").forward(request, response);
	}
}
