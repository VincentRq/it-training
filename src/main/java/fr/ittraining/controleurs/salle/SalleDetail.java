package fr.ittraining.controleurs.salle;

import java.io.IOException;
import java.util.List;

import fr.ittraining.modeles.managers.SalleManager;
import fr.ittraining.modeles.objets.Salle;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/salleDetail")
public class SalleDetail extends HttpServlet{

	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Salle salle = new SalleManager().findById(Integer.valueOf(request.getParameter("salleId")));
		request.setAttribute("salle", salle);
		this.getServletContext().getRequestDispatcher("/WEB-INF/salle/salleDetail.jsp").forward(request, response);
	}
}

