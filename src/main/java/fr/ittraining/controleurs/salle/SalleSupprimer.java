package fr.ittraining.controleurs.salle;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import fr.ittraining.modeles.managers.SalleManager;
import fr.ittraining.modeles.objets.Salle;

@WebServlet(urlPatterns="/salleSupprimer")
public class SalleSupprimer extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SalleManager salleManager = new SalleManager();
		int id = Integer.valueOf(request.getParameter("id"));
		Salle salle = salleManager.findById(id);
		request.setAttribute("messageSucces", "La salle " + salle.getNom() + " a �t� supprim�e");
		salleManager.supprimer(id);
		response.sendRedirect(request.getContextPath() + "/salleListe");
	}
}
