package fr.ittraining.controleurs.salle;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import fr.ittraining.modeles.managers.SalleManager;
import fr.ittraining.modeles.objets.Salle;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/salleAjouterModifier")
public class SalleAjouterModifier extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/salle/salleAjouterModifier.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Boolean formulairePasse;
		if(request.getParameter("formulairePasse") != null) {
			formulairePasse = Boolean.valueOf(request.getParameter("formulairePasse"));
		} else {
			formulairePasse = false;
		}
		String nom = request.getParameter("nom");
		int capacite = 0;
		if(request.getParameter("capacite") != null) {
			capacite = Integer.valueOf(request.getParameter("capacite"));
		}
		String adresse = request.getParameter("adresse");
		String codePostal = request.getParameter("codePostal");
		String ville = request.getParameter("ville");
		String pays = request.getParameter("pays");
		
		SalleManager salleManager = new SalleManager();
		
		Map<String, String> erreurs = salleManager.verif(nom, capacite, adresse, codePostal, ville, pays);
		
		if(formulairePasse) {
			if(erreurs.isEmpty()) { // Formulaire pass� sans erreur --> On cr�e la salle
				Salle salle = new Salle(nom, capacite, adresse, codePostal, ville, pays);
				if(request.getParameter("id") == null) { 	// Il s'agit d'une cr�ation --> On ajoute une ligne � la BDD
					salleManager.enregistrer(salle);
					request.setAttribute("messageSucces", "La nouvelle salle a bien �t� cr��e");
				} else {									// Il s'agit d'une modification --> On modifie une ligne de la BDD
					int id = Integer.valueOf(request.getParameter("id"));
					salleManager.modifier(salle, id);
					request.setAttribute("messageSucces", "La salle a bien �t� modifi�e");
				}
				response.sendRedirect(request.getContextPath() + "/salleListe");
			} else {				// Formulaire pass� avec erreur --> on renvoie au formulaire avec les erreurs
				request.setAttribute("erreurs", erreurs);
				this.getServletContext().getRequestDispatcher("/WEB-INF/salle/salleAjouterModifier.jsp").forward(request, response);
			}
		} else if (!formulairePasse) { // Formulaire non pass� malgr� requ�te POST --> envoie vers le formulaire pour modifier les champs
			this.getServletContext().getRequestDispatcher("/WEB-INF/salle/salleAjouterModifier.jsp").forward(request, response);
		}
	}
}
