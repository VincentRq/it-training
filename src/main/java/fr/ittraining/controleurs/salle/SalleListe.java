package fr.ittraining.controleurs.salle;

import java.io.IOException;
import java.util.List;

import fr.ittraining.modeles.managers.SalleManager;
import fr.ittraining.modeles.objets.Salle;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/salleListe")
public class SalleListe extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Salle> salles = new SalleManager().findAll();
		request.setAttribute("salles", salles);
		this.getServletContext().getRequestDispatcher("/WEB-INF/salle/salleListe.jsp").forward(request, response);
	}
}
