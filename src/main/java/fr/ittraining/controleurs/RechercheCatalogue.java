package fr.ittraining.controleurs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.ittraining.modeles.managers.CatalogueManager;
import fr.ittraining.modeles.objets.Catalogue;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/rechercheCatalogue")
public class RechercheCatalogue extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String recherche = request.getParameter("recherche");
		List<Catalogue> catalogues = new CatalogueManager().selectionnerParPertinence(recherche);
		request.setAttribute("catalogues", catalogues);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/rechercheCatalogue.jsp").forward(request, response);
	}
}
