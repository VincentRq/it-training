package fr.ittraining.controleurs.catalogue;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import fr.ittraining.modeles.managers.CatalogueManager;
import fr.ittraining.modeles.objets.Catalogue;

@WebServlet(urlPatterns="/catalogueSupprimer")
public class CatalogueSupprimer extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CatalogueManager catalogueManager = new CatalogueManager();
		int id = Integer.valueOf(request.getParameter("id"));
		Catalogue catalogue = catalogueManager.findById(id);
		request.setAttribute("messageSucces", "La formation " + catalogue.getTitre() + " a �t� supprim�e du catalogue");
		catalogueManager.supprimer(id);
		response.sendRedirect(request.getContextPath() + "/catalogueListe");
	}
}
