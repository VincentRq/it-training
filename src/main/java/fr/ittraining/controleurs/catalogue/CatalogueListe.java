package fr.ittraining.controleurs.catalogue;

import java.io.IOException;
import java.util.List;

import fr.ittraining.modeles.managers.CatalogueManager;
import fr.ittraining.modeles.objets.Catalogue;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/catalogueListe")
public class CatalogueListe extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Catalogue> catalogues = new CatalogueManager().findAll();
		request.setAttribute("catalogues", catalogues);
		this.getServletContext().getRequestDispatcher("/WEB-INF/catalogue/catalogueListe.jsp").forward(request, response);
	}
}
