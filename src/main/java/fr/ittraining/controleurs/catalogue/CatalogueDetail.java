package fr.ittraining.controleurs.catalogue;

import java.io.IOException;
import java.util.List;

import fr.ittraining.modeles.managers.CatalogueManager;
import fr.ittraining.modeles.objets.Catalogue;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/catalogueDetail")
public class CatalogueDetail extends HttpServlet{

	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Catalogue catalogue = new CatalogueManager().findById(Integer.valueOf(request.getParameter("catalogueId")));
		request.setAttribute("catalogue", catalogue);
		System.out.println("Le catalogue : " + catalogue);
		this.getServletContext().getRequestDispatcher("/WEB-INF/catalogue/catalogueDetail.jsp").forward(request, response);
	}
}

