package fr.ittraining.controleurs.catalogue;

import java.io.IOException;
import java.util.Map;

import fr.ittraining.modeles.managers.CatalogueManager;
import fr.ittraining.modeles.objets.Catalogue;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/catalogueAjouterModifier")
public class CatalogueAjouterModifier extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/catalogue/catalogueAjouterModifier.jsp").forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Boolean formulairePasse;
		if(request.getParameter("formulairePasse") != null) {
			formulairePasse = Boolean.valueOf(request.getParameter("formulairePasse"));
		} else {
			formulairePasse = false;
		}
		String titre = request.getParameter("titre");
		String domaine = request.getParameter("domaine");
		String theme = request.getParameter("theme");
		String sousTheme = request.getParameter("sousTheme");
		int dureeH = 0;
		if (request.getParameter("dureeH") != null) {
			dureeH = Integer.valueOf(request.getParameter("dureeH"));
		}
		double prix = 0;
		if (request.getParameter("prix") != null || request.getParameter("prix") != "") {
			prix = Double.valueOf(request.getParameter("prix"));
		}
		int nbStagiaireMin = 0;
		if (request.getParameter("nbStagiaireMin") != null) {
			nbStagiaireMin = Integer.valueOf(request.getParameter("nbStagiaireMin"));
		}
		int nbStagiaireMax = 0;
		if (request.getParameter("nbStagiaireMax") != null) {
			nbStagiaireMax = Integer.valueOf(request.getParameter("nbStagiaireMax"));
		}
		String prerequis = "";
		if (request.getParameter("prerequis") != null) {
			prerequis = request.getParameter("prerequis");
		}
		
		String description = "";
		if (request.getParameter("description") != null) {
			description = request.getParameter("description");
		}
		String programme = "";
		if (request.getParameter("programme") != null) {
			programme = request.getParameter("programme");
		}
		
		CatalogueManager catalogueManager = new CatalogueManager();
		
		
		Map<String, String> erreurs = catalogueManager.verif(titre, domaine, theme, sousTheme, dureeH, prix, nbStagiaireMin, nbStagiaireMax, prerequis);

		if(formulairePasse) {
			if(erreurs.isEmpty()) { // Formulaire pass� sans erreur --> On cr�e la salle
				Catalogue catalogue = new Catalogue(titre, domaine, theme, sousTheme, dureeH, prix, nbStagiaireMin, nbStagiaireMax, prerequis, description, programme);
				System.out.println("Catalogue � Modifier ou Ajouter : " + catalogue);
				if(request.getParameter("id") == null) { 	// Il s'agit d'une cr�ation --> On ajoute une ligne � la BDD
					catalogueManager.enregistrer(catalogue);
					request.setAttribute("messageSucces", "le nouveau mod�le de formation a bien �t� ajout� au catalogue");
				} else {									// Il s'agit d'une modification --> On modifie une ligne de la BDD
					int id = Integer.valueOf(request.getParameter("id"));
					catalogueManager.modifier(catalogue, id);
					request.setAttribute("messageSucces", "Le catalogue a bien �t� modifi�");
				}
				response.sendRedirect(request.getContextPath() + "/catalogueListe");
			} else {				// Formulaire pass� avec erreur --> on renvoie au formulaire avec les erreurs
				request.setAttribute("erreurs", erreurs);
				this.getServletContext().getRequestDispatcher("/WEB-INF/catalogue/catalogueAjouterModifier.jsp").forward(request, response);
			}
		} else if (!formulairePasse) { // Formulaire non pass� malgr� requ�te POST --> envoie vers le formulaire pour modifier les champs
			this.getServletContext().getRequestDispatcher("/WEB-INF/catalogue/catalogueAjouterModifier.jsp").forward(request, response);
		}
	}
}
