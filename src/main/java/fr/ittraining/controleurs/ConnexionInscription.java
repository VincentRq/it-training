package fr.ittraining.controleurs;

import java.io.IOException;
import java.util.Map;

import fr.ittraining.modeles.managers.UtilisateurManager;
import fr.ittraining.modeles.objets.Utilisateur;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns="/connexionInscription")
public class ConnexionInscription extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/connexionInscription.jsp").forward(request, response);
	}
		
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if (request.getParameter("inscription") != null) {
			String email = request.getParameter("email");
			String entreprise = request.getParameter("entreprise");
			String mdp = request.getParameter("mdp");
			String verifMdp = request.getParameter("verifMdp");
			
			UtilisateurManager utilisateurManager = new UtilisateurManager();
			
			Map<String,String> erreurs = utilisateurManager.verif(email, mdp, verifMdp);
						
			if (erreurs.isEmpty()) {
				Utilisateur utilisateur = new Utilisateur(email, entreprise, mdp);
				utilisateurManager.enregistrerUtilisateur(utilisateur);
				request.setAttribute("messageSuccess", "Vous �tes bien inscrit");
				this.getServletContext().getRequestDispatcher("/WEB-INF/connexionInscription.jsp")
				.forward(request, response);
			} else {
				request.setAttribute("erreurs", erreurs);
				this.getServletContext().getRequestDispatcher("/WEB-INF/connexionInscription.jsp")
				.forward(request, response);
			} 
		}
		
		if(request.getParameter("connexion") != null) {
			String email = request.getParameter("email");
			String mdp = request.getParameter("mdp");
			UtilisateurManager utilisateurManager = new UtilisateurManager();
			Utilisateur utilisateur = utilisateurManager.findByEmailAndMdp(email, mdp);
			
			if(utilisateur == null) {
				request.setAttribute("messageErreurConnexion", "Email ou mot de passe invalide");
				this.getServletContext().getRequestDispatcher("/WEB-INF/connexionInscription.jsp")
				.forward(request, response);
			} else {
				
				request.getSession().setAttribute("utilisateur", utilisateur);
				this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp")
				.forward(request, response);
			}
			
		
		}
		
		
	}
	
}
