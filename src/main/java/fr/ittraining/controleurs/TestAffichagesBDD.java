package fr.ittraining.controleurs;

import java.io.IOException;
import java.util.List;

import fr.ittraining.modeles.managers.CatalogueManager;
import fr.ittraining.modeles.managers.SalleManager;
import fr.ittraining.modeles.objets.Catalogue;
import fr.ittraining.modeles.objets.Salle;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


@WebServlet(urlPatterns="/testAffichagesBDD")
public class TestAffichagesBDD extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Catalogue> catalogues = new CatalogueManager().findAll();
		List<Salle> salles = new SalleManager().findAll();
		request.setAttribute("catalogues", catalogues);
		request.setAttribute("salles", salles);
		this.getServletContext().getRequestDispatcher("/WEB-INF/testAffichagesBDD.jsp").forward(request, response);
	}
}
